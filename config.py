#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : September 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - November 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : config -> get OS and set configuration 
                @&&&&              #               
                   @&&             # 
'''

import sys, platform 

# define deploy or not 
deploy = False   

### Do not change following code ###

# get current OS 
# OS = 'Windows' or 'Linux' or ''
OS = platform.system()

#create base path of our package 
base_path = '.'

if deploy:
    if OS == 'Windows':
        base_path = './lib' #base_path = sys._MEIPASS 
    elif OS == 'Linux':
        base_path = './lib'

