#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : April 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : main -> application launcher
                @&&&&              #               
                   @&&             # 
'''

import os, sys  

import src.logger.logger as logger 
sys.stdout = logger.logger()
sys.sterr = logger.logger()

os.environ["TQDM_DISABLE"] = "1"
os.environ['KIVY_LOG_MODE'] = 'PYTHON'

import src.wrapper as wrapper  

import signal  
import config 

def lock(file_locker):
    if os.path.exists(file_locker):
      return False  
    else:
      f = open(file_locker, 'w')
      return True 
    
def unlock(file_locker):
    os.remove(file_locker)


if __name__ == '__main__':
    file_locker = config.base_path + '/src/utils/.locker.lock'
    try:
      if lock(file_locker):        
        #launch wrapper 
        wrapper.main()
        
        # create and launch ESP device
        wrapper.launch_deviceESP()
        # launch IHM 
        wrapper.launch_IHM()
        # unlock application for future launch
        if config.OS == 'Windows': 
          unlock(file_locker)
        # save user's progress 
        wrapper.main_app.progress.save()
        # kill java gateway 
        wrapper.terminate_java_process(wrapper.main_app.java_proc)

        PID = os.getpid()
        if config.OS == 'Windows':
            os.kill(PID, signal.SIGTERM)
      else:
          sys.exit()
    finally:
       unlock(file_locker)
