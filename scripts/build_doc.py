#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : June 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - November 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : build documentation
                @&&&&              #               
                   @&&             # 
'''

# import modules
import os

if __name__ == '__main__':
    files = ['./src/wrapper.py', \
            #'./src/dummy/dummy.py', \
            './src/logger/logger.py', \
            './src/speechReco/speech2text.py', \
            #'./src/wordPrediction/wordPrediction.py', \
            #'./src/nextWordPrediction/nextWordPrediction.py', \
            './src/windowManager/windowManager.py', \
            #'./src/keycodeEncoderDecoder/keycodeEncoderDecoder.py', \
            './src/text2Speech/text2Speech.py', \
            './src/profil/profil.py', \
            './src/progress/progress.py', \
            './src/predict4All/wordPredict4all.py', \
            './src/IHM_helper/IHM_helper.py', \
            './src/IHM_helper/physicalKeyboard.py', \
            './src/IHM_helper/deviceProfil.py', \
            './src/IHM_helper/userHelp.py', \
            './src/IHM_helper/userProfil.py', \

            './test/test_wrapper.py', \
            #'./test/dummy/test_dummy.py', \
            './test/speechReco/test_speech2text.py', \
            #'./test/wordPrediction/test_wordPrediction.py', \
            #'./test/nextWordPrediction/test_nextWordPrediction.py', \
            #'./test/keycodeEncoderDecoder/test_keycodeEncoderDecoderpy'
            ]

    command = 'pydoctor --project-name=libellueVirtualKeyboard \
                        --project-base-dir="." \
                        --make-html \
                        --html-output=doc '

    for file in files:
        command += '{} '.format(file)

    os.system(command)
