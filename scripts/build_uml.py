#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : June 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - November 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : build UML diagrams 
                @&&&&              #               
                   @&&             # 
'''

# import modules
import os

if __name__ == '__main__':
    
    #os.system('export PYTHONPATH="${PYTHONPATH}:${PWD}/src"')

    files = ['./main.py', \
            './src/wrapper.py', \
            './src/IHM_helper/IHM_helper.py', \
            './src/IHM_helper/physicalKeyboard.py', \
            './src/IHM_helper/deviceProfil.py', \
            './src/IHM_helper/userHelp.py', \
            './src/IHM_helper/userProfil.py']#, \
            #'./src/IHM_helper/virtualKeyboard.py']

    basis = 'pyreverse -o png '
    options = ' -f ALL -my -S --colorized'

    os.makedirs('./uml', exist_ok=True)

    if 0: # generate one uml for each file 
        for file in files:
            name = file.split('/')[-1].split('.')[0]
            command = '{} {} {}'.format(basis, file, options)
            os.system(command)
            os.system('mv *.png uml/{}.png'.format(name))


    # generate UML for whole project 
    all_files = ''
    for file in files:
        all_files += '{} '.format(file)
    command = '{} {} {}'.format(basis, all_files, options)
    os.system(command)
    os.system('mv *.png ./uml/')
    #os.system('mv packages.png ./uml/packages.png')
