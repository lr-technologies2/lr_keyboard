# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Dossier : scripts  

#### Objectifs 

Collecter les scripts d'intégration continue du projet 

#### Usages 

##### Documentation 

Générer un site web accessible depuis `./doc/index.html` contenant tout les attributs et méthodes des classes du projet 

```
python3 ./scripts/build_doc.py 
```
> Nécessite de documenter le code via les docstrings 

##### Tests et Couverture de code  

* Exécuter les tests unitaires du projet afin de valider les fonctionnalités 
* La couverture de code est accessible depuis `./htmlcov/index.html` 

```
python3 ./scripts/build_tests.py 
```
> Nécessite de développer les fonctions de test 
>> Il est possible de "mock" un test afin de forcer la sortie d'une fonction  
>> Il est possible d'ajouter `# pragma: no cover` pour ne pas prendre en compte une ligne de code dans la couverture 

##### Diagramme de classes UML  

Générer un diagramme de classes du projet 

```
python3 ./scripts/build_uml.py 
```

##### Tout exécuter 

Le script `run_validators.py` situé à la racine du projet permet d'exécuter les trois scripts précédents. 
```
python3 ./run_validators.py 
```
> Il est indispensable d'exécuter ce script durant le développement de fonctionnalités afin de garantir l'avancée du projet. 