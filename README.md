# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap.  

## Auteurs  

* Edouard VILLAIN (evillain@lrtechnologies.fr)  
* Adrien DORISE (adorise@lrtechnologies.fr) 
* Boris LENSEIGNE (blenseigne@lrtechnologies.fr)  
* Julia COHEN (jcohen@lrtechnologies.fr)   

> Contributeurs  
>> Les libelliens 

### Architecture logicielle  

📦lalibellulekeyboard  
 ┣ 📂doc  
 ┣ 📂htmlcov  
 ┣ 📂scripts  
 ┣ 📂src  
 ┃ ┣ 📂IHM_helper  
 ┃ ┃ ┣ 📂img  
 ┃ ┣ 📂dummy  
 ┃ ┣ 📂keycodeEncoderDecoder  
 ┃ ┣ 📂logger  
 ┃ ┣ 📂nextWordPrediction  
 ┃ ┃ ┣ 📂data  
 ┃ ┣ 📂predict4All  
 ┃ ┃ ┣ 📂data  
 ┃ ┃ ┣ 📂lib  
 ┃ ┣ 📂profil  
 ┃ ┣ 📂progress  
 ┃ ┣ 📂speechReco  
 ┃ ┣ 📂text2Speech  
 ┃ ┣ 📂windowManager  
 ┃ ┣ 📂wordPrediction  
 ┃ ┗ 📜wrapper.py  
 ┣ 📂test  
 ┃ ┣ 📂dummy  
 ┃ ┣ 📂keycodeEncoderDecoder  
 ┃ ┣ 📂nextWordPrediction  
 ┃ ┣ 📂predict4All  
 ┃ ┣ 📂speechReco  
 ┃ ┣ 📂wordPrediction  
 ┃ ┗ 📜test_wrapper.py  
 ┣ 📂uml  
 ┣ 📜main.py  
 ┗ 📜run_validators.py  

* Le dossier `doc` contient la documentation  
* Le dossier `htmlcov` contient la couverture du code  
* Le dossier `scripts` contient les scripts de validation du projet  
* Le dossier `src` contient le code source du projet (organisé en composants)  
* Le dossier `test` contient les tests unitaires du projet (par composant)  
* Le dossier `uml` contient le diagramme de classes du projet  
* Le fichier `main.py` permet d'exécuter le projet  
* Le fichier `run_validators.py` permet d'exécuter les scripts de validation du projet   

### Usages 

Dans le fichier `config.py` il est possible d'activer le mode deploiement ou non 
> mettre la variable `deploy` du fichier `config.py` à `True` ou `False`  
> Le mode debug est automatiquement desactivé en mode déployé 

Pour exécuter l'application : `python3 main.py`  
Pour exécuter la validation de l'application : `python3 run_validators.py`  

### Deploiement Linux  

Pour déployer le paquet sous linux, utiliser la commande suivante :  
`python3 setup.py build`  
Un dossier `build` sera crée contenant un dossier `exe.linux-x86_64-3.10`  
Le dossier `build/lib` contient les librairies python utilisées, notre paquet ainsi que les meta data  
> Le maintient du fichier setup.py est à la charge du développeur lors de la modification du paquet  

Double cliquer sur l'executable `main` pour executer le projet  
Zipper le dossier `exe.linux-x86_64-3.10` pour partager le projet sous Linux  

### Deploiement Windows  

Pour déployer le paquet sous Windows, utiliser la commande suivante :  
`python3 setup.py bdist_msi`  
Le dossier `dist` contient l'installeur Windows à partager  
Un dossier `build` sera crée contenant un dossier `exe.win32-amd64-3.10`  
Le dossier `build/lib` contient les librairies python utilisées, notre paquet ainsi que les meta data  
> Le maintient du fichier setup.py est à la charge du développeur lors de la modification du paquet  

Il est possible d'utiliser la commande `python3 setup.py build` pour ne pas générer l'installeur  

### Particularités 

* Un wrapper instancie tout les composants situé dans le dossier `src`  
* Créer une branche pour développer une nouvelle fonctionnalité  
* Exécuter le script `run_validators.py` pour valider votre développement  
* Si la validation passe, créer une merge request.  
> Mettre relecteur au minimum l'un des auteurs du projet  