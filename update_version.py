#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : January 2024
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : update version script -> update version in python header files 
                @&&&&              #                            
                   @&&             # 
'''

# import module
import os

def update_version(file, old_version, new_version):
    try:
        # open file, read and close file 
        f = open(file, 'r')
        txt = f.read()
        f.close()
        # replace version 
        txt = txt.replace(old_version, new_version)
        # write in same file 
        f = open(file, 'w')
        f.write(txt)
        f.close()
    except:
        print('Cannot update {} file'.format(file))

def main(path, old_version, new_version):
    # list of excluded folders 
    excluded_folders = ['__pycache__', 'doc', 'uml', '.git']
    #print('Updating files in {} folder'.format(path))
    # list files 
    files = os.listdir(path)
    # foreach files 
    for file in files:
        # if isFile and is *.py file 
        if os.path.isfile(path + file):
            if file.endswith('.py') or file.endswith('.sh'):
                # update version 
                update_version(path + file, old_version, new_version)
        # if folder not excluded 
        if os.path.isdir(path + file):
            if not file in excluded_folders:
                # recursive call 
                main(path + file + '/', old_version, new_version)


if __name__ == '__main__':
    # set your version  
    old_version = 'v-1.0.0'
    new_version = 'v-1.0.0'

    main('./', old_version, new_version)
