# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Tests du composant wordPrediction            

| Tests | Résultats |
|:------|:-----------:|
| `test_wordPrediction_init()` | :white_check_mark: |
| `test_wordPrediction_predict()`  | :white_check_mark: |  

## Résultats  

| Tests | Couverture | 
|:-----:|:----------:|
| 100 % | 100 %      | 



