#!/usr/bin/env python3
# coding: utf-8

# license : open sources
# Author Villain Edouard
"""test for wordPrediction module"""

# import modules
import pytest

import src.wordPrediction.wordPrediction as wordPrediction 
import src.wrapper as wrapper 

wrapper.main()

def test_wordPrediction_init():
    """test module wordPrediction, function __init__"""
    nb_words = 4
    wp = wordPrediction.wordPrediction(nb_words)

    assert wp.filepath == "./src/wordPrediction/data/", \
        'filepath not properly set'
    assert wp.wordfilepath == "./src/wordPrediction/data/liste_de_frequence_mots.ods", \
        'wordfilepath not properly set'
    assert wp.nb_words == nb_words, \
        'nb_words not properly set'

    del wp
        
        
def test_wordPrediction_predict():
    """test module wordPrediction, function predict """
    nb_words = 5
    wp = wordPrediction.wordPrediction(nb_words)
    input = 'sa'
    ret = wp.predict(input)
    expected = ['sans', 'savoir', 'saint', 'sang', 'salle']
    
    assert len(ret) == nb_words, \
        'wordPrediction predict size should be {}'.format(nb_words)

    assert ret == expected, \
        'expected prediction with sa as input should be {}'.format(expected)

    del wp


