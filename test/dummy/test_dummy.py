#!/usr/bin/env python3
# coding: utf-8

# license : open sources
# Author Villain Edouard
"""test for dummy module"""

# import modules
import pytest

import src.dummy.dummy as dummy 
import src.wrapper as wrapper 

wrapper.main()

def test_dummy_init():
    """test module dummy, function __init__"""
    d = dummy.dummy(1)

    assert d.arg is not None, 'd.arg uninitialized'
    assert d.arg == 1, 'd.arg initialized with wrong value'


def test_dummy_myFunc_equal():
    """test module dummy, function myFunc equal"""
    equal = 1
    d = dummy.dummy(equal)
    # d.myFunc() will compare high and low
    assert d.myFunc(equal) == True, 'd.myFunc should be True'

def test_dummy_myFunc_not_equal():
    """test module dummy, function myFunc not equal"""
    not_equal = 1
    d = dummy.dummy(0)
    # d.myFunc() will compare high and low
    assert d.myFunc(not_equal) == False, 'd.myFunc should be False'
