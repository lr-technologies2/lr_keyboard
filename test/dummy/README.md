# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Tests du composant dummy    

| Tests | Résultats |
|:------|:-----------:|
| `test_dummy_init()`  | :white_check_mark: |
| `test_dummy_myFunc_equal()`  | :white_check_mark: |
| `test_dummy_myFunc_not_equal()`  | :white_check_mark: |

## Résultats  

| Tests | Couverture | 
|:------|:-----------|
| 100 % | 100 %      | 


