#!/usr/bin/env python3
# coding: utf-8

# license : open sources
# Author Villain Edouard
"""test for speech2text module"""

# import modules
import pytest

import queue
import vosk
import threading

import src.speechReco.speech2text as speech2text 
import src.wrapper as wrapper 

wrapper.main()

def test_speech2text_init():
    """test module speech2text, function __init__"""
    s2t = speech2text.speech2text()

    assert isinstance(s2t.q, queue.Queue)
    assert s2t.device is None
    assert isinstance(s2t.device_info, dict)
    assert isinstance(s2t.samplerate, int)
    assert isinstance(s2t.model, vosk.Model)
    assert s2t.rec is None
    assert s2t.data is None
    assert s2t.listening == False   
    assert s2t.t_listen is None  

    del s2t

def test_speech2text_handle_speech_with_reco(mocker):
    """test module speech2text, function handle_speech when user say Bonjour and it's recognized"""
    s2t = speech2text.speech2text()
    mocker.patch('src.speechReco.speech2text.speech2text.get_res_dict', return_value='Bonjour')
    ret = s2t.handle_speech()

    assert ret == 'Bonjour', "expected return value = 'Bonjour'"

    del s2t

def test_speech2text_handle_speech_without_reco(mocker):
    """test module speech2text, function handle_speech when nothing happen"""
    s2t = speech2text.speech2text()
    mocker.patch('src.speechReco.speech2text.speech2text.get_res_dict', return_value='')
    ret = s2t.handle_speech()

    assert ret == "", \
        "expected return value = ''"

def test_speech2text_handle_speech_without_anything(mocker):
    """test module speech2text, function handle_speech when nothing happen"""
    s2t = speech2text.speech2text()
    mocker.patch('src.speechReco.speech2text.speech2text.get_res_dict', return_value=None)
    ret = s2t.handle_speech()
    assert ret == ""

    del s2t


