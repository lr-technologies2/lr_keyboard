# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Tests du composant speech2Text          

| Tests | Résultats |
|:------|:-----------:|
| `test_speech2text_init()` | :white_check_mark: |
| `test_speech2text_handle_speech_with_reco()`  | :white_check_mark: |  
| `test_speech2text_handle_speech_without_reco()` | :white_check_mark: |
| `test_speech2text_handle_speech_without_anything()`  | :white_check_mark: |  

## Résultats  

| Tests | Couverture | 
|:-----:|:----------:|
| 100 % | 100 %      | 



