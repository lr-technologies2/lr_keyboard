# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap.  

### Liste des composants et tests  

| Composant   |      Fonctions      | Tests | Couverture | 
|:----------|:--------------|:-------------:|:-------------:|
| dummy | composant d'exemple mais inutile | :white_check_mark: | 100 % | 
| keycodeEncodeDecoder | Conversion évenement clavier <-> caractère | :white_check_mark: | 100 % | 
| logger | trace des évenements survenus | :x: | 100 % | 
| nextWordPrediction | Prédiction du prochain mot à partir des phrases précédente | :white_check_mark: | 100 % | 
| predict4All | Correction orthographique et prédiction du prochain mot | :white_check_mark: | 100 % | 
| profil | Gestion du profil utilisateur | :x: | 50 % | 
| progress | Gestion de l'historique d'utilisation des outils | :x: | 57 % | 
| speech2Text | Générer une chaine de caractères à partir de l'audio | :white_check_mark: | 100 % | 
| text2Speech | Générer l'audio à partir une chaine de caractères | :x: | 100 % | 
| windowManager | Liste fenêtres / applications ouvertes et focus | :x: | 57 % | 
| wordPrediction | Prédire la prochaine lettre | :white_check_mark: | 100 % | 
| wrapper | instancier les composants | :white_check_mark: | 100 % | 


### Tests du wrapper              

| Tests | Résultats |
|:------|:-----------:|
| `test_wrapper_init_debug_True()` | :white_check_mark: |
| `test_wrapper_init_debug_False()`  | :white_check_mark: |  
| `test_wrapper_launch_java()`  | :white_check_mark: |  

| Tests | Couverture | 
|:-----:|:----------:|
| 100 % | 100 %      | 