#!/usr/bin/env python3
# coding: utf-8

# license : open sources
# Author Villain Edouard
"""test for wordPrediction module"""

# import modules
import pytest

import src.keycodeEncoderDecoder.keycodeEncoderDecoder as keycodeEncoderDecoder 
import src.wrapper as wrapper 

wrapper.main()

def test_keycodeEncoderDecoder_init():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    assert isinstance(ked.to_ignore, dict), \
        'error, to_ignore is not set'
        
    assert isinstance(ked.upper, dict), \
        'error, upper is not set'
        
    assert isinstance(ked.ctrl_alt, dict), \
        'error, ctrl_alt is not set'
        
    assert isinstance(ked.non_numeric, dict), \
        'error, non_numeric is not set'
        
    assert len(ked.to_ignore) != 0, \
        'error, to_ignore is empty'
        
    assert len(ked.upper) != 0, \
        'error, upper is empty'
        
    assert len(ked.ctrl_alt) != 0, \
        'error, ctrl_alt is empty'
        
    assert len(ked.non_numeric) != 0, \
        'error, non_numeric is empty'
    
def test_decode_a():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 97, 20, 'a', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'a', \
        'error, should decode a'
        
def test_decode_z():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 122, 26, 'z', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'z', \
        'error, should decode z'
        
def test_decode_e():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 101, 8, 'e', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'e', \
        'error, should decode e'
        
def test_decode_r():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 114, 21, 'r', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'r', \
        'error, should decode r'
        
def test_decode_t():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 116, 23, 't', [])
    char = ked.decode(pseudo_keycode)
    assert char == 't', \
        'error, should decode t'
        
def test_decode_y():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 121, 28, 'y', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'y', \
        'error, should decode y'  

def test_decode_u():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 117, 24, 'u', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'u', \
        'error, should decode u'         
        
def test_decode_i():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 105, 12, 'i', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'i', \
        'error, should decode i'
        
def test_decode_o():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 111, 18, 'o', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'o', \
        'error, should decode o'  

def test_decode_p():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 112, 19, 'p', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'p', \
        'error, should decode p'    

def test_decode_q():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 113, 4, 'q', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'q', \
        'error, should decode q'
        
def test_decode_s():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 115, 22, 's', [])
    char = ked.decode(pseudo_keycode)
    assert char == 's', \
        'error, should decode s'
        
def test_decode_d():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 100, 7, 'd', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'd', \
        'error, should decode d'
        
def test_decode_f():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 102, 9, 'f', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'f', \
        'error, should decode f'
        
def test_decode_g():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 103, 10, 'g', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'g', \
        'error, should decode g'
        
def test_decode_h():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 104, 11, 'h', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'h', \
        'error, should decode h'  

def test_decode_j():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 106, 13, 'j', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'j', \
        'error, should decode j'         
        
def test_decode_k():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 107, 14, 'k', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'k', \
        'error, should decode k'
        
def test_decode_l():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 108, 15, 'l', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'l', \
        'error, should decode l'  

def test_decode_m():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 109, 51, 'm', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'm', \
        'error, should decode m'  

def test_decode_w():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 119, 29, 'w', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'w', \
        'error, should decode w'
        
def test_decode_x():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 120, 27, 'x', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'x', \
        'error, should decode x'  

def test_decode_c():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 99, 6, 'c', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'c', \
        'error, should decode c'         
        
def test_decode_v():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 118, 25, 'v', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'v', \
        'error, should decode v'
        
def test_decode_b():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 98, 5, 'b', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'b', \
        'error, should decode b'  

def test_decode_n():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 110, 17, 'n', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'n', \
        'error, should decode n'  

def test_decode_A():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 97, 20, 'a', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'A', \
        'error, should decode A'
        
def test_decode_Z():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 122, 26, 'z', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'Z', \
        'error, should decode Z'
        
def test_decode_E():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 101, 8, 'e', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'E', \
        'error, should decode E'
        
def test_decode_R():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 114, 21, 'r', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'R', \
        'error, should decode R'
        
def test_decode_T():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 116, 23, 't', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'T', \
        'error, should decode T'
        
def test_decode_Y():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 121, 28, 'y', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'Y', \
        'error, should decode Y'  

def test_decode_U():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 117, 24, 'u', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'U', \
        'error, should decode U'         
        
def test_decode_I():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 105, 12, 'i', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'I', \
        'error, should decode I'
        
def test_decode_O():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 111, 18, 'o', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'O', \
        'error, should decode O'  

def test_decode_P():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 112, 19, 'p', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'P', \
        'error, should decode P'    

def test_decode_Q():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 113, 4, 'q', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'Q', \
        'error, should decode Q'
        
def test_decode_S():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 115, 22, 's', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'S', \
        'error, should decode S'
        
def test_decode_D():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 100, 7, 'd', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'D', \
        'error, should decode D'
        
def test_decode_F():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 102, 9, 'f', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'F', \
        'error, should decode F'
        
def test_decode_G():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 103, 10, 'g', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'G', \
        'error, should decode G'
        
def test_decode_H():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 104, 11, 'h', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'H', \
        'error, should decode H'  

def test_decode_J():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 106, 13, 'j', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'J', \
        'error, should decode J'         
        
def test_decode_K():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 107, 14, 'k', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'K', \
        'error, should decode K'
        
def test_decode_L():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 108, 15, 'l', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'L', \
        'error, should decode L'  

def test_decode_M():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 109, 51, 'm', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'M', \
        'error, should decode M'  

def test_decode_W():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 119, 29, 'w', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'W', \
        'error, should decode W'
        
def test_decode_X():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 120, 27, 'x', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'X', \
        'error, should decode X'  

def test_decode_C():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 99, 6, 'c', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'C', \
        'error, should decode C'         
        
def test_decode_V():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 118, 25, 'v', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'V', \
        'error, should decode V'
        
def test_decode_B():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 98, 5, 'b', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'B', \
        'error, should decode B'  

def test_decode_N():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 110, 17, 'n', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'N', \
        'error, should decode N'  

def test_decode_A_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 97, 20, 'a', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'A', \
        'error, should decode A'
        
def test_decode_Z_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 122, 26, 'z', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'Z', \
        'error, should decode Z'
        
def test_decode_E_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 101, 8, 'e', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'E', \
        'error, should decode E'
        
def test_decode_R_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 114, 21, 'r', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'R', \
        'error, should decode R'
        
def test_decode_T_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 116, 23, 't', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'T', \
        'error, should decode T'
        
def test_decode_Y_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 121, 28, 'y', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'Y', \
        'error, should decode Y'  

def test_decode_U_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 117, 24, 'u', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'U', \
        'error, should decode U'         
        
def test_decode_I_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 105, 12, 'i', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'I', \
        'error, should decode I'
        
def test_decode_O_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 111, 18, 'o', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'O', \
        'error, should decode O'  

def test_decode_P_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 112, 19, 'p', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'P', \
        'error, should decode P'    

def test_decode_Q_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 113, 4, 'q', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'Q', \
        'error, should decode Q'
        
def test_decode_S_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 115, 22, 's', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'S', \
        'error, should decode S'
        
def test_decode_D_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 100, 7, 'd', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'D', \
        'error, should decode D'
        
def test_decode_F_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 102, 9, 'f', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'F', \
        'error, should decode F'
        
def test_decode_G_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 103, 10, 'g', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'G', \
        'error, should decode G'
        
def test_decode_H_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 104, 11, 'h', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'H', \
        'error, should decode H'  

def test_decode_J_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 106, 13, 'j', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'J', \
        'error, should decode J'         
        
def test_decode_K_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 107, 14, 'k', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'K', \
        'error, should decode K'
        
def test_decode_L_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 108, 15, 'l', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'L', \
        'error, should decode L'  

def test_decode_M_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 109, 51, 'm', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'M', \
        'error, should decode M'  

def test_decode_W_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 119, 29, 'w', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'W', \
        'error, should decode W'
        
def test_decode_X_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 120, 27, 'x', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'X', \
        'error, should decode X'  

def test_decode_C_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 99, 6, 'c', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'C', \
        'error, should decode C'         
        
def test_decode_V_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 118, 25, 'v', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'V', \
        'error, should decode V'
        
def test_decode_B_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 98, 5, 'b', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'B', \
        'error, should decode B'  

def test_decode_N_capslock():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 110, 17, 'n', ['capslock'])
    char = ked.decode(pseudo_keycode)
    assert char == 'N', \
        'error, should decode N' 

def test_non_numeric_1():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 49, 30, '1', [])
    char = ked.decode(pseudo_keycode)
    assert char == '&', \
        'error, should decode &'
        
def test_non_numeric_2():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 50, 31, '2', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'é', \
        'error, should decode é'
        
def test_non_numeric_3():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 51, 32, '3', [])
    char = ked.decode(pseudo_keycode)
    assert char == '"', \
        'error, should decode "'
        
def test_non_numeric_4():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 52, 33, '4', [])
    char = ked.decode(pseudo_keycode)
    assert char == '\'', \
        'error, should decode \''

def test_non_numeric_5():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 53, 34, '5', [])
    char = ked.decode(pseudo_keycode)
    assert char == '(', \
        'error, should decode ('
        
def test_non_numeric_6():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 54, 35, '6', [])
    char = ked.decode(pseudo_keycode)
    assert char == '-', \
        'error, should decode -'

def test_non_numeric_7():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 55, 36, '7', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'è', \
        'error, should decode è'
        
def test_non_numeric_8():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 56, 37, '8', [])
    char = ked.decode(pseudo_keycode)
    assert char == '_', \
        'error, should decode _'

def test_non_numeric_9():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 57, 38, '9', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'ç', \
        'error, should decode ç'
        
def test_non_numeric_0():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 48, 39, '0', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'à', \
        'error, should decode à'
        
def test_non_numeric_10():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 41, 45, ')', [])
    char = ked.decode(pseudo_keycode)
    assert char == ')', \
        'error, should decode )'
        
def test_non_numeric_11():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 61, 46, '=', [])
    char = ked.decode(pseudo_keycode)
    assert char == '=', \
        'error, should decode ='

def test_non_numeric_1_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 49, 30, '1', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '1', \
        'error, should decode 1'
        
def test_non_numeric_2_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 50, 31, '2', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '2', \
        'error, should decode 2'
        
def test_non_numeric_3_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 51, 32, '3', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '3', \
        'error, should decode 3'
        
def test_non_numeric_4_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 52, 33, '4', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '4', \
        'error, should decode 4'

def test_non_numeric_5_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 53, 34, '5', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '5', \
        'error, should decode 5'
        
def test_non_numeric_6_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 54, 35, '6', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '6', \
        'error, should decode 6'

def test_non_numeric_7_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 55, 36, '7', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '7', \
        'error, should decode 7'
        
def test_non_numeric_8_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 56, 37, '8', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '8', \
        'error, should decode 8'

def test_non_numeric_9_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 57, 38, '9', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '9', \
        'error, should decode 9'
        
def test_non_numeric_0_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 48, 39, '0', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '0', \
        'error, should decode 0'
        
def test_non_numeric_10_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 41, 45, ')', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '°', \
        'error, should decode °'
        
def test_non_numeric_11_shift():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 61, 46, '=', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '+', \
        'error, should decode +'
        
        
def test_special_caracter_0():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 44, 16, ',', [])
    char = ked.decode(pseudo_keycode)
    assert char == ',', \
        'error, should decode ,'    
        
def test_special_caracter_1():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 59, 54, ';', [])
    char = ked.decode(pseudo_keycode)
    assert char == ';', \
        'error, should decode ;'      

def test_special_caracter_2():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 58, 55, ':', [])
    char = ked.decode(pseudo_keycode)
    assert char == ':', \
        'error, should decode :'    
        
def test_special_caracter_3():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 33, 56, '!', [])
    char = ked.decode(pseudo_keycode)
    assert char == '!', \
        'error, should decode !'  
        
def test_special_caracter_4():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 249, 52, 'ù', [])
    char = ked.decode(pseudo_keycode)
    assert char == 'ù', \
        'error, should decode ù'    
        
def test_special_caracter_5():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 42, 49, '*', [])
    char = ked.decode(pseudo_keycode)
    assert char == '*', \
        'error, should decode *'  
        
def test_special_caracter_6():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 1073741824, 47, None, [])
    char = ked.decode(pseudo_keycode)
    assert char == '^', \
        'error, should decode ^'    
        
def test_special_caracter_7():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 36, 48, '$', [])
    char = ked.decode(pseudo_keycode)
    assert char == '$', \
        'error, should decode $'       
        
        








def test_special_caracter_0_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 44, 16, ',', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '?', \
        'error, should decode ?'    
        
def test_special_caracter_1_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 59, 54, ';', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '.', \
        'error, should decode .'      

def test_special_caracter_2_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 58, 55, ':', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '/', \
        'error, should decode /'    
        
def test_special_caracter_3_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 33, 56, '!', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '§', \
        'error, should decode §'  
        
def test_special_caracter_4_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 249, 52, 'ù', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '%', \
        'error, should decode %'    
        
def test_special_caracter_5_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 42, 49, '*', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == 'µ', \
        'error, should decode µ'  
        
def test_special_caracter_6_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 1073741824, 47, None, ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '¨', \
        'error, should decode ¨'    
        
def test_special_caracter_7_shift():   
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 36, 48, '$', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '£', \
        'error, should decode £'           































    
        
        
        
def test_ctrl_alt_2():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 50, 31, '2', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '~', \
        'error, should decode ~'      
        
def test_ctrl_alt_3():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 51, 32, '3', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '#', \
        'error, should decode #'         
        
def test_ctrl_alt_4():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 52, 33, '4', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '{', \
        'error, should decode {'      
        
def test_ctrl_alt_5():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 53, 34, '5', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '[', \
        'error, should decode ['   
        
def test_ctrl_alt_6():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 54, 35, '6', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '|', \
        'error, should decode |'      
        
def test_ctrl_alt_7():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 55, 36, '7', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '`', \
        'error, should decode `'   
        
def test_ctrl_alt_8():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 56, 37, '8', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '\\', \
        'error, should decode \\'      
        
def test_ctrl_alt_9():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 57, 38, '9', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '^', \
        'error, should decode ^'   
        
def test_ctrl_alt_0():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 48, 39, '0', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '@', \
        'error, should decode @'      
        
def test_ctrl_alt_10():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 41, 45, ')', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == ']', \
        'error, should decode ]'   
        
def test_ctrl_alt_11():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 61, 46, '=', ['alt', 'ctrl'])
    char = ked.decode(pseudo_keycode)
    assert char == '}', \
        'error, should decode }'      
        
def test_del():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 8, 42, None, [])
    char = ked.decode(pseudo_keycode)
    assert char == 'del', \
        'error, should decode del'
        
def test_to_ignore():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 304, 225, 'İ', ['shift'])
    char = ked.decode(pseudo_keycode)
    assert char == '', \
        'error, should decode '
        
def test_left():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 276, 80, None, [])
    char = ked.decode(pseudo_keycode)
    assert char == '', \
        'error, should decode '

def test_down():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 274, 81, None, [])
    char = ked.decode(pseudo_keycode)
    assert char == '', \
        'error, should decode '
        
def test_right():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 275, 79, None, [])
    char = ked.decode(pseudo_keycode)
    assert char == '', \
        'error, should decode '
        
def test_up():
    ked = keycodeEncoderDecoder.keycodeEncoderDecoder()
    
    pseudo_keycode = (None, 273, 82, None, [])
    char = ked.decode(pseudo_keycode)
    assert char == '', \
        'error, should decode '