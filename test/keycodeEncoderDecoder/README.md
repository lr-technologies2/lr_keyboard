# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Tests du composant keycodeEncoderDecoder      

| Tests | Résultats |
|:------|:-----------:|
| `test_keycodeEncoderDecoder_init()` | :white_check_mark: |
| `test_decode()`  | :white_check_mark: |  
| `test_decode_shift()`  | :white_check_mark: |  
| `test_decode_capslock()`  | :white_check_mark: |  
| `test_del()`  | :white_check_mark: |  
| `test_non_numeric()`  | :white_check_mark: |  
| `test_numeric()`  | :white_check_mark: |  
| `test_to_ignore()`  | :white_check_mark: |  
| `test_ctrl_alt()`  | :white_check_mark: |  
| `test_upper()`  | :white_check_mark: |  

## Résultats  

| Tests | Couverture | 
|:-----:|:----------:|
| 100 % | 100 %      | 


