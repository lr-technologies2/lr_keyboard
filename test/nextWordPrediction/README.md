# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Tests du composant nextWordPrediction        

| Tests | Résultats |
|:------|:-----------:|
| `test_nextWordPrediction_init()` | :white_check_mark: |
| `test_nextWordPrediction_predict_unable_to_find()`  | :white_check_mark: |  
| `test_nextWordPrediction_predict()`  | :white_check_mark: |  
| `test_nextWordPrediction_add_sentence()`  | :white_check_mark: |  

## Résultats  

| Tests | Couverture | 
|:-----:|:----------:|
| 100 % | 100 %      | 


