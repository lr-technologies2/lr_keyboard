#!/usr/bin/env python3
# coding: utf-8

# license : open sources
# Author Villain Edouard
"""test for nextWordPrediction module"""

# import modules
import pytest
import os 

import src.nextWordPrediction.nextWordPrediction as nextWordPrediction 
import src.wrapper as wrapper 

wrapper.main()

def test_nextWordPrediction_init():
    """test module nextWordPrediction, function __init__"""
    nb_words = 5
    nwp = nextWordPrediction.nextWordPrediction(nb_words)

    assert nwp.data_path == './src/nextWordPrediction/data/dataset.txt', \
        'data_path not properly set'
    assert nwp.lexicon == {}, \
        'lexicon not properly set'
    assert nwp.nb_words == nb_words, \
        'nb_words not properly set'

    del nwp
    
def test_nextWordPrediction_predict_unable_to_find():
    """test module wordPrediction, function predict """
    nb_words = 5
    nwp = nextWordPrediction.nextWordPrediction(nb_words)
    nwp.data_path = './src/nextWordPrediction/data/dataset_test.txt'
    input = 'zigwigwi'
    nwp.fill_lexicon()
    nwp.compute_probabilities()
    ret = nwp.predict(input)
    expected = ['', '', '', '', '']
    
    assert len(ret) == nb_words, \
        'wordPrediction predict size should be {}'.format(nb_words)

    assert ret == expected, \
        'expected prediction with sa as input should be {}'.format(expected)

    del nwp
        
def test_nextWordPrediction_predict():
    """test module wordPrediction, function predict """
    nb_words = 5
    nwp = nextWordPrediction.nextWordPrediction(nb_words)
    nwp.data_path = './src/nextWordPrediction/data/dataset_test.txt'
    input = 'je'
    nwp.fill_lexicon()
    nwp.compute_probabilities()
    ret = nwp.predict(input)
    expected = ['suis', 'serais', 'pense', '', '']
    
    assert len(ret) == nb_words, \
        'wordPrediction predict size should be {}'.format(nb_words)

    assert ret == expected, \
        'expected prediction with sa as input should be {}'.format(expected)

    del nwp


def test_nextWordPrediction_add_sentence():
    """test module wordPrediction, function predict add_sentence_to_lexical"""
    # create a temporary copy of dataset_test.txt 
    os.system('cp ./src/nextWordPrediction/data/dataset_test.txt ./src/nextWordPrediction/data/dataset_test__.txt')
    nb_words = 5
    nwp = nextWordPrediction.nextWordPrediction(nb_words)
    nwp.data_path = './src/nextWordPrediction/data/dataset_test__.txt'
    nwp.line_max = 5
    sentence = "il était une fois"
    expected = "je suis là\nje suis devant\nje serais\nje pense donc je suis\ntout à l'heure\nil était une fois\n"
    
    nwp.add_sentence_to_lexical(sentence)
    ret = open(nwp.data_path, 'r').read()
    
    assert ret == expected, \
        'dataset should contain {} but get {}'.format(expected, ret)
    
    # delete temporary file 
    os.system('rm ./src/nextWordPrediction/data/dataset_test__.txt')
    del nwp 