#!/usr/bin/env python3
# coding: utf-8

# license : open sources
# Author Villain Edouard
"""test for wrapper"""

# import modules
import pytest

import src.dummy.dummy as dummy 
import src.speechReco.speech2text as speech2text 
import src.logger.logger as logger 
import src.wrapper as wrapper 

'''def test_wrapper_init_debug_True():
    """test wrapper init"""
    debug = True 
    log_conf = './src/logger/logger.conf'
    w = wrapper.wrapper(log_conf, debug)

    assert isinstance(w.logger, logger.logger), 'logger uninitialized'
    assert isinstance(w.dummy, dummy.dummy), 'dummy uninitialized'
    assert isinstance(w.speech2text, speech2text.speech2text), 'speech2text uninitialized'
    assert w.debug == True, 'debug should be True'

def test_wrapper_init_debug_False():
    """test wrapper init"""
    debug = False 
    log_conf = './src/logger/logger.conf'
    w = wrapper.wrapper(log_conf, debug)

    assert isinstance(w.logger, logger.logger), 'logger uninitialized'
    assert isinstance(w.dummy, dummy.dummy), 'dummy uninitialized'
    assert isinstance(w.speech2text, speech2text.speech2text), 'speech2text uninitialized'
    assert w.debug == False, 'debug should be False'

def test_wrapper_launch_java():
    """test java launcher"""
    pid = wrapper.launch_javaConnector()
    wrapper.terminate_java_process(pid)'''