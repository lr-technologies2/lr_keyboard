#!/usr/bin/env python3
# coding: utf-8

# license : open sources
# Author Villain Edouard
"""test for wrapper"""

# import modules
import pytest

import src.predict4All.wordPredict4all as  wordPredict4all
import src.wrapper as wrapper 

def test_predict4All_no_space():
    wp       = wordPredict4all.wordPredict4all()
    input    = 'bonjour'
    expected = ['bonjour', 'bon jour']

    ret = wp.predict(input)
    for i in range(len(expected)):
        assert ret[i] == expected[i], \
        'error, should predict {} but get {}'.format(expected[i], ret[i])

def test_predict4All_space():
    wp       = wordPredict4all.wordPredict4all()
    input    = 'bonjour '
    expected = ['je', 'à', 'monsieur', 'Mme', 'les']

    ret = wp.predict(input)
    for i in range(len(expected)):
        assert ret[i] == expected[i], \
        'error, should predict {} but get {}'.format(expected[i], ret[i])
