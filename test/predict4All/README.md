# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Tests du composant predict4All          

| Tests | Résultats |
|:------|:-----------:|
| `test_predict4All_no_space()` | :white_check_mark: |
| `test_predict4All_space()`  | :white_check_mark: |  

## Résultats  

| Tests | Couverture | 
|:-----:|:----------:|
| 100 % | 100 %      | 


