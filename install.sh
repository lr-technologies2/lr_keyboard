#!/bin/bash

##################################
#       /     &&                _     ____    _____         _                 _             _            
#        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
#          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
#               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
#                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
#                 .#@@&                                                               |___/              
#                   @@@/       
#            .&.   (@@,//           ######################################################
#           &&&&  &(@@@//           # Licence     : MIT
#           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
#           @  //(@@@@/*            # Created     : January 2024
#       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
#      @@@/(((// @@/*               # version     : v-1.0.0 
#   %.@@@@@@/////&&                 #  
#     &@&&  //  &,%&&               # module : install bash script -> install executable for Linux 
#                @&&&&              #                            
#                   @&&             # 


base_path="/home/$USER"
company_name="LR_Technologies"
application_name="Koi_rect_me"
root_package=$PWD 

desktop_entry_0="[Desktop Entry]"
desktop_entry_1="Version=1.0.0"
desktop_entry_2="Type=Application"
desktop_entry_3="Name=Koi rect me"
desktop_entry_4="Icon=$base_path/$company_name/$application_name/logo_LR.ico"
desktop_entry_5="Path=$base_path/$company_name/$application_name"
desktop_entry_6="Exec=\"$base_path/$company_name/$application_name/main\" %U"
desktop_entry_7="Comment=\"Intelligent application developped by LR Technologies R&D team\""
desktop_entry_8="Categories=Utility"
desktop_entry_9="Terminal=false"

function print_logo
{
    echo "       /     &&               "
    echo "        *&&(&&#*              "
    echo "          ,&&##%*              _     ____    _____         _                 _             _           "
    echo "               #%%@.          | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___ "
    echo "                 %%@(         | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _\` | |/ _ \\/ __|"
    echo "                 .#@@&        | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\\__ \\"
    echo "                   @@@/       |_____|_| \\_\\   |_|\\___|\\___|_| |_|_| |_|\\___/|_|\\___/ \\__, |_|\\___||___/"
    echo "            .&.   (@@,//                                                             |___/       "
    echo "           &&&&  &(@@@//      "
    echo "           && @/(/@@,///      "
    echo "           @  //(@@@@/*       "
    echo "       @@@@((/(@@@@@(#&%&&&   "
    echo "      @@@/(((// @@/*          "
    echo "   %.@@@@@@/////&&            "
    echo "     &@&&  //  &,%&&          "
    echo "                @&&&&         "
    echo "                   @&&        "
}


function create_desktop_entry 
{
    echo "$(date) : Creating desktop entry in $1"
    echo $desktop_entry_0 > $1
    echo $desktop_entry_1 >> $1
    echo $desktop_entry_2 >> $1
    echo $desktop_entry_3 >> $1
    echo $desktop_entry_4 >> $1
    echo $desktop_entry_5 >> $1
    echo $desktop_entry_6 >> $1
    echo $desktop_entry_7 >> $1
    echo $desktop_entry_8 >> $1
    echo $desktop_entry_9 >> $1
}

function start_installing 
{
    # creating folders 
    cd $base_path
    mkdir $company_name 
    cd $company_name
    mkdir $application_name
    cd $application_name 


    # copying file from root_package to current pwd 
    cp $root_package/koi_rect_me.zip koi_rect_me.zip 
    # unzip package 
    unzip -qq koi_rect_me.zip
    
    # create desktop entry
    create_desktop_entry $base_path/Desktop/koi_rect_me.desktop
    create_desktop_entry $base_path/.local/share/applications/koi_rect_me.desktop
}

function clean_post_installation 
{
    rm $base_path/$company_name/$application_name/koi_rect_me.zip
    #rm $root_package/Koi_rect_me.zip
}

function start_uninstalling 
{
    echo "$(date) : removing Koi rect me application"
    rm -rf $base_path/$company_name/$application_name
    echo "$(date) : removing Koi rect me shortcuts"
    rm $base_path/Desktop/koi_rect_me.desktop
    rm $base_path/.local/share/applications/koi_rect_me.desktop
}

function main {
    print_logo 
    echo "$(date) : Koi rect me installer"
    echo "$(date) : Installation folder ->  $base_path/$company_name/$application_name" 


    # ask user confirmation 
    read -p "$(date) : Do you want to install, uninstall or exit ? (install/uninstall/exit) " yn

    case $yn in 
        install )   start_installing;
                    clean_post_installation;;
        uninstall ) start_uninstalling;;
        exit )      echo "$(date) : Canceling installation..." ;
                    exit 2;;
        * )         echo invalid response;
                    exit 1;;
    esac
}

main 