#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : August 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - August 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : installation script -> for dev mode inside virtual environment 
                @&&&&              #               
                   @&&             # 
'''

import os 

if __name__ == '__main__':
    print('*****  LR Technologies *****')
    print(' Welcome to LR keyboard, the intelligent keyboard')
    print(' I will install dependencies')
    print(' I will use super user rights, and ask your password')
    
    os.system('sudo apt-get install python3-dev \
                                    python3-tk \
                                    libcairo2-dev \
                                    libjpeg-dev \
                                    libpango1.0-dev \
                                    libgif-dev \
                                    build-essential \
                                    graphviz \
                                    xclip \
                                    libgirepository1.0-dev \
                                    libportaudio2 \
                                    default-jre \
                                    default-jdk')
    
    print(' I will install python packages')
    os.system('pip3 install -r requirements.txt')
    
    print(' Finished...')
    print(' Enjoy...')