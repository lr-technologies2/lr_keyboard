# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : speech2text  

#### Objectifs 

Utiliser la reconnaissance vocale pour générer la chaine de caractères correspondant à la phrase dictée par l'utilisateur .  

#### Usages 

La fonction `handle_speech()` accessible depuis le wrapper permet la reconnaissance vocale de bout en bout. 

* Utilisation du module `sounddevice` pour enregistrer l'utilisateur 
* Utilisation du module `Vosk` pour la reconnaissance vocale 

```
str = wrapper.main_app.speech2text.handle_speech()
```

#### Particularités 

* Le bouton de reconnaissance vocale de l'IHM doit être grisé pour confirmer à l'utilisateur que l'enregistrement est en court 
* Freeze l'application durant la reconnaissance vocale 
