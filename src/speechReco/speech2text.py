#!/usr/bin/env python3

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : May 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - May 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : speech to text -> speech to text component 
                @&&&&              #               
                   @&&             # 
'''

# import module 
from vosk import SetLogLevel
SetLogLevel(-1)

import queue
import sys
import sounddevice as sd
import json
import vosk

import src.wrapper as wrapper 


class speech2text:
    """class speech2text : used vosk and Kaldi to generate string from audio and micro"""
    def __init__(self, lang="fr"):
        self.q = queue.Queue()
        """queue.Queue: used by callback in order to store data from recognition"""
        self.device = None
        """default microphone used"""
        self.device_info = sd.query_devices(self.device, "input")
        """dict : store microphone infos"""
        # soundfile expects an int, sounddevice provides a float:
        self.samplerate = int(self.device_info["default_samplerate"])
        """int: samplerate of your microphone"""
        self.model = vosk.Model(lang=lang)
        """vosk.Model: model used for speech recognition"""
        self.rec = None
        """rec: used to store KaldiRecognizer"""
        self.data = None
        """data: used to store queue.Queue"""
        self.text = ""
        """text: used to store text recognized"""
        self.listening = False 
        """boolean: do we listen ? """ 
        self.t_listen = None 
        """thread: listen in background"""

    def callback(self,indata, frames, time, status):
        """This is called (from a separate thread) for each audio block."""
        if status:                          # pragma: no cover
            print("Displaying status")      # pragma: no cover
            print(status, file=sys.stderr)  # pragma: no cover
        self.q.put(bytes(indata))           # pragma: no cover

    def get_res_dict(self):
        """get_res_dict: load result json and return text"""
        res_dict = json.loads(self.rec.Result())    # pragma: no cover
        return res_dict['text']                     # pragma: no cover


    def handle_speech(self):
        """handle_speech: """
        wrapper.main_app.speech_recognized = ''
        with sd.RawInputStream(samplerate=self.samplerate, blocksize = 8000, device=self.device, dtype="int16", channels=1, callback=self.callback):
            self.rec = vosk.KaldiRecognizer(self.model, self.samplerate)
            while True:
                self.data = self.q.get()
                if self.rec.AcceptWaveform(self.data):
                    res_dict_text = self.get_res_dict()
                    if res_dict_text is not None:
                        wrapper.main_app.logger.logger.debug("[speech2text] - recognized : {}".format(res_dict_text))
                        wrapper.main_app.speech_recognized = res_dict_text 
                        return 
                    else:
                        wrapper.main_app.logger.logger.debug("[speech2text] - Nothing recognized, self.text set to empty")
                        return 


