# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap.  

## Composant : deviceESP    

#### Objectifs 

Gestion des périphériques liés à une carte ESP32 

#### Usages 

La librairie sensorlib provient du projet libelluleSensorlib de LR Technologies.  
Ce projet se focalise sur la création et l'utilisation de périphériques adaptatifs gérant les contrôles d'un ordinateur (curseur, click, touche clavier...)

#### Particularités 

* Seul la version Linux est actuellement développée   

