#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Adrien Dorise (adorise@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : December 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Adrien Dorise - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : deviceESP -> interface with sensorlib module 
                @&&&&              #               
                   @&&             # 
'''

# import modules
import src.wrapper as wrapper 

import threading
import time
import sensorlib.devices.tools as device_tools
import sensorlib.backend.communication as comm
import config


class deviceESP():
    """class deviceESP"""
    def __init__(self):
        """ Init custom ESP device """
        self.calib_file_path = config.base_path + '/src/utils/lr_calib.json' 
        self.current_calibration_instruction = "Initialising calibration"
        self.device_thread = None 
        self.device = None
        self.load_config()
        self.interrupt_thread = False
        self.is_doing_calibration = False
        if(self.active_device):
            self.create_device()
            self.start_device()
    
    def create_device(self):
        """ Create device object """
        self.load_config()
        self.sampling_time = 0.01
        self.device = device_tools.create_device(device_name=self.device_used_name, cursor_speed=self.cursor_speed, sampling_time=self.sampling_time, communication_type=self.communication_type, blt_device_name=self.bluetooth_name, serial_port=self.USB_name)
        self.device.load_config(self.calib_file_path)
        
    def start_device(self):
        self.device_thread = threading.Thread(target=self.device_loop)
        self.device_thread.start()       
    
    def device_loop(self):
        """Device loop that is to be handled by the device_thread"""
        self.interrupt_thread = False
        while not self.interrupt_thread:
            wrapper.main_app.logger.logger.debug("[deviceESP] - Establishing communication with device")

            error_flag = self.connect()
            
            if(error_flag >= 0):
                wrapper.main_app.logger.logger.debug("[deviceESP] - Communication with device established")
                self.control()
                wrapper.main_app.logger.logger.debug("[deviceESP] - Communication with device lost")

            time.sleep(1)
        wrapper.main_app.logger.logger.debug("[deviceESP] - Device thread stopped")

    def load_config(self):
        """ Load device configuration """
        wrapper.main_app.profil.load_profil()
        profil                  = wrapper.main_app.profil.profil[wrapper.main_app.profil.user_name]
        self.USB_name           = profil['USB_name']
        self.bluetooth_name     = profil['bluetooth_name']
        self.cursor_speed       = profil['cursor_speed']
        self.active_USB         = profil['active_USB']
        self.active_bluetooth   = profil['active_bluetooth']
        self.active_device      = not profil['device_used_name'] == 'Pas de capteur'
        self.device_used_name   = profil['device_used_name']
        
        self.communication_type = comm.CommunicationType.USB # Default is USB
        if(self.active_USB):
            self.communication_type = comm.CommunicationType.USB
        elif(self.active_bluetooth):
            self.communication_type = comm.CommunicationType.BLUETOOTH
    
    def control(self):
        """ Control loop to link the device with computer's actions """
        return self.device.control()
    
    def connect(self):
        """ Instantiate the link between the PC and the ESP32 """
        return self.device.connect()
        
    def calibrate(self):
        """ Calibrate the device's sensors """   
        if(self.device is None or self.is_doing_calibration or not self.device.comm_connected):
            wrapper.main_app.logger.logger.debug("[deviceESP] - No device connected to the application: calibration impossible")
            return -1     
        self.is_doing_calibration = True
        self.current_calibration_instruction = "Initialising calibration"
        self.kill_thread()
        for token ,instr in self.device.calibration_instructions.items():
            self.current_calibration_instruction = instr
            error = self.device.calibrate({token: instr})
            if(error < 0):
                wrapper.main_app.logger.logger.debug("[deviceESP] - Calibration failed")
                self.is_doing_calibration = False
                self.destroy_device()
                self.create_device()
                self.start_device()
                return -1
        
        self.device.save_config(self.calib_file_path)
        self.is_doing_calibration = False
        self.destroy_device()
        self.create_device()
        self.start_device()
        
        return 0
    
    
    def update_cursor_speed(self, new_speed):
        """ Update the speed at which the cursor is moved """
        if(self.active_device):
            self.device.cursor_speed = new_speed
        
        
    def kill_thread(self):
        if self.device_thread.is_alive():
            self.interrupt_thread = True
            self.device.interrupt = True
            self.device_thread.join()
            self.interrupt_thread = False
            self.device.interrupt = False
        
    def destroy_device(self):
        """ Destroy the device object and stop the related thread """
        if(self.device is not None):
            self.interrupt_thread = True
            self.device.interrupt = True
            self.device_thread.join()
            self.device.disconnect()
            self.device.comm_connected = False
            self.device_thread = None
            self.device = None