
'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : November 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : HMI physical keyboard -> HMI basis
                @&&&&              #               
                   @&&             # 
'''

from kivy.config import Config
Config.set('kivy', 'keyboard_mode', 'system')

# It’s required that the base Class
# of your App inherits from the App class.
from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty, ListProperty 
from kivy.uix.button import Button
from functools import partial
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout  
from kivy.uix.popup import Popup 
from kivy.uix.textinput import TextInput
from kivy import require
from kivy.clock import Clock 
import screeninfo
require("1.8.0")
import pyautogui
import pyclip 
Window.softinput_mode = ""
import threading 
from kivy.animation import Animation
import src.wrapper as wrapper 

import config  
Builder.load_file(config.base_path + '/src/IHM_helper/physicalKeyboard.kv')
    

# This class stores the info of .kv file
# when it is called goes to my.kv file
class appHelper_physicalKeyboard(Screen):
    """class appHelper_physicalKeyboard : kivy IHM """
    displayLabel            = ObjectProperty()
    kbContainer             = ObjectProperty()
    kbContainerPred4All     = ObjectProperty()
    kbContainerWindows      = ObjectProperty()
    font_size               = NumericProperty()
    nb_pred                 = NumericProperty()
    available_user = ListProperty()
    
    def __init__(self, **kwargs):
        """init kivi app """
        self.button_color = (193/255,211/255,254/255,127/255)
        #self.available_user = ['defaut']
        self.load_profil()
        #self.select_profil()
        super(appHelper_physicalKeyboard, self).__init__(**kwargs)
        self.icon_path1 = config.base_path + '/src/IHM_helper/img/microphone.png'
        self.icon_path2 = config.base_path + '/src/IHM_helper/img/options.png'
        self.icon_path3 = config.base_path + '/src/IHM_helper/img/help.png'
        self.icon_path4 = config.base_path + '/src/IHM_helper/img/device_options.png'
        self.icon_path5 = config.base_path + '/src/IHM_helper/img/go_back.png'
        self.icon_path6 = config.base_path + '/src/IHM_helper/img/refresh.png'
        self.icon_path7 = config.base_path + '/src/IHM_helper/img/delete.png'
        self.icon_path8 = config.base_path + '/src/IHM_helper/img/select_profil.png'
        self.icon_path9 = config.base_path + '/src/IHM_helper/img/add_user.png'
        self.icon_path10 = config.base_path + '/src/IHM_helper/img/delete_user.png'
        self.icon_path11 = config.base_path + '/src/IHM_helper/img/information.png'

        self.last_text = ""

        self._add_pred4All_Button()
        self.nextWord   = ['' for i in range(self.nb_pred)]
        self.nextLetter = ['' for i in range(self.nb_pred)]
        # bind physical keyboard on_key_down
        Window.bind(on_key_up=self._on_keyboard_up)
        # update opened windows 
        self.update_active_window()
        
        
    def resize(self):
        monitor_size, monitor_pos = self.get_monitor_size()    
        Window.size = (monitor_size[0]*0.55, monitor_size[1] * 0.2)
        
        self.load_profil()
        wrapper.main_app.IHM_helper.title = 'Koï rect me - {}'.format(wrapper.main_app.profil.user_name)
        # update widget with new font size 
        self.remove_active_window()
        self.update_active_window()
        self._add_pred4All_Button()
        
    def get_monitor_size(self):
        """get monitor size """
        for m in screeninfo.get_monitors():
            if m.is_primary is True:
                scr_width  = m.width
                scr_height = m.height
                scr_posX   = m.x
                scr_posY   = m.y
        return (scr_width, scr_height), (scr_posX, scr_posY)
    


    def update_user_spinner(self):
        wrapper.main_app.profil.set_user(self.ids.spinner_names.text)
        wrapper.main_app.logger.logger.debug("[appHelper_physicalKeyboard] - selected user : {}".format(wrapper.main_app.profil.user_name))
        self.resize()

    def delete_user(self):
        if not wrapper.main_app.profil.user_name == 'Utilisateur':
            self.available_user.remove(wrapper.main_app.profil.user_name) 
            wrapper.main_app.profil.delete_user(wrapper.main_app.profil.user_name, self.available_user)
            wrapper.main_app.profil.user_name = 'Utilisateur'
            self.resize()

    def add_user(self):
        """ add user to profil
        """
        self.user_name_textInput = TextInput(text='')
        layout = BoxLayout(orientation="vertical")
        content_save = Button(text='Ajouter', size_hint_y=None)
        content_cancel = Button(text='Annuler', size_hint_y=None)
        layout_btn = BoxLayout(orientation="horizontal")
        layout_btn.add_widget(content_cancel)
        layout_btn.add_widget(content_save)
        layout.add_widget(self.user_name_textInput)
        layout.add_widget(layout_btn)

        self.popup_ = Popup(
            title="Tapez votre pseudonyme :",
            content=layout, 
            size_hint=(None, None), size=(270, 300),
            auto_dismiss = False, 
            background_color = (0, 0, 0, 1)
        )
        content_cancel.bind(on_release=self.popup_.dismiss)
        content_save.bind(on_release=self.add_new_user)
        
        self.popup_.open() 

    def add_new_user(self, *args):
        wrapper.main_app.profil.set_user(self.user_name_textInput.text) 
        wrapper.main_app.profil.add_user(wrapper.main_app.profil.user_name)
        wrapper.main_app.logger.logger.debug("[appHelper_physicalKeyboard] - add new user : {}".format(wrapper.main_app.profil.user_name))
        self.popup_.dismiss()

        self.resize()
    
    def load_profil(self):
        wrapper.main_app.profil.load_profil() 
        profil                  = wrapper.main_app.profil.profil[wrapper.main_app.profil.user_name]
        self.active_predict4All = profil['active_predict4All']
        self.active_nextWord    = profil['active_nextWord']
        self.active_enter       = profil['active_enter']
        self.active_voice       = profil['active_voice']
        self.font_size          = profil['font_size']
        self.nb_pred            = profil['nb_pred']
        self.USB_name           = profil['USB_name']
        self.bluetooth_name     = profil['bluetooth_name']
        self.cursor_speed       = profil['cursor_speed']
        
        self.available_user = [*wrapper.main_app.profil.profil]
        
        wrapper.main_app.logger.logger.debug("[appHelper_physicalKeyboard] - profil loaded -> {}".format(profil))
        
    def update_active_window(self):
        """upade opened window """
        wrapper.main_app.windowManager.update_opened_windows()
        # add a button foreach window 
        for name in wrapper.main_app.windowManager.l_windows_name:
            if not "@!" in name and not 'BDHF' in name and not wrapper.main_app.IHM_helper.title in name:
                if len(name) > 50: 
                    txt_name = name[:50] + ' ...'
                else:
                    txt_name = name
                b = Button(
                        text=txt_name,
                        font_size=self.font_size, 
                        on_release=partial(self.flush, name ),
                        color=(0, 0, 0, 1),
                        background_color=self.button_color,
                        halign='left')
                self.kbContainerWindows.add_widget(b)
            
    def remove_active_window(self):
        """clear widget containing windows buttons """
        self.ids.kbContainerWindows.clear_widgets()
            
    def flush(self, *args):
        """write content inside selected window"""
        self.last_text = self.displayLabel.text
        # set focus 
        wrapper.main_app.windowManager.set_active_window(args[0])
        # copy text inside buffer 
        pyclip.copy(self.displayLabel.text.replace('  ', ' '))
        # paste 
        pyautogui.hotkey("ctrl", "v")
        # if checkbox enter is active press enter 
        if self.active_enter:
            pyautogui.press('enter')
        # empty text 
        self.displayLabel.text = ''
        # set focus on our application 
        # update opened windows 
        self.remove_active_window()
        self.update_active_window()
        wrapper.main_app.windowManager.set_active_window(wrapper.main_app.IHM_helper.title)

    def _on_keyboard_up(self, *args):
        """on keyboard down : catch event when keyboard is used"""           
        wrapper.main_app.progress.nb_typed_letter += 1
        # if check box for prediction is active, predict 
        if not self.displayLabel.text == '':
            if self.displayLabel.focus:
                if self.active_predict4All:
                    self.predict_predict4All()
            char = self.displayLabel.text[-1]
            if self.active_voice:
                if char != 'del':
                    wrapper.main_app.text2Speech.tts(char, 'fr')
                    
               
    def start_reco(self):
        self.ids.but_dictee.disabled = True 
        threading.Thread(target=self.button_dictee).start()
        self.ids.but_dictee.disabled = False 
        self.displayLabel.text += wrapper.main_app.speech_recognized + ' ' 
        self.last_text = self.displayLabel.text
        
        wrapper.main_app.progress.nb_letter_dictated += len(wrapper.main_app.speech_recognized)
        if self.active_voice:
            wrapper.main_app.text2Speech.tts(self.displayLabel.text, 'fr')
        # predict next word 
        if self.active_predict4All:
            self.predict_predict4All()
    
    def _button_dictee(self):
        self.ids.but_dictee.disabled = True 
        self.recognition_thread = threading.Thread(target=wrapper.main_app.speech2text.handle_speech)
        self.recognition_thread.start()
        # Créer une animation pour le bouton
        self.animation = Animation(background_color=(1, 0, 0, 1), duration=0.5) + \
                    Animation(background_color=(1, 1, 1, 1), duration=0.5)
        
        # Répéter l'animation pendant la reconnaissance
        self.animation.repeat = True
        self.animation.start(self.ids.but_dictee)
        
        self.test = Clock.schedule_interval(lambda dt: self.check_thread_status(self.recognition_thread), 0.5)
        
        
    def check_thread_status(self, thread):
        # Réactiver le bouton si le thread est terminé
        if not thread.is_alive():
            self.ids.but_dictee.disabled = False
            # Arrêter de surveiller une fois que le thread est terminé
            Clock.unschedule(self.test)
            self.animation.stop(self.ids.but_dictee)
            self.ids.but_dictee.background_color = self.button_color
            self.displayLabel.text += wrapper.main_app.speech_recognized + ' ' 
            self.last_text = self.displayLabel.text
            
            wrapper.main_app.progress.nb_letter_dictated += len(wrapper.main_app.speech_recognized)
            if self.active_voice:
                wrapper.main_app.text2Speech.tts(self.displayLabel.text, 'fr')
            # predict next word 
            if self.active_predict4All:
                self.predict_predict4All()      
        
    def predict_predict4All(self):
        """predict next letter"""
        nextWord = wrapper.main_app.wordPredict4all.predict(self.displayLabel.text)
        for i in range(self.nb_pred):
            if nextWord.size() > i:
                self.button_predict4All[i].text = nextWord[i]
        
    def _add_pred4All_Button(self):
        """ Add a buttons for each pred """
        self.ids.kbContainerPred4All.clear_widgets()
        self.button_predict4All = []
        for i in range(self.nb_pred):
            self.button_predict4All.append(
                Button(
                    text='',
                    font_size=self.font_size, 
                    background_color=self.button_color,
                    color=(0, 0, 0, 1),
                    on_release=partial(self.set_pred_predict4All))
            )
            self.kbContainerPred4All.add_widget(self.button_predict4All[-1])
        
    def set_pred_predict4All(self, button):
        """add selected word to text"""
        index = self.displayLabel.text.rfind(" ")
        # last words
        last_words = self.displayLabel.text[:index+1]
        self.displayLabel.text = u"{}{} ".format(last_words,button.text)
        wrapper.main_app.progress.nb_letter_completed += len(button.text)
        self.last_text = self.displayLabel.text
        
        if self.active_voice:
            wrapper.main_app.text2Speech.tts(button.text, 'fr')
        # if next word prediction is active, predict 
        if self.active_predict4All:
            self.predict_predict4All()


    def go_back(self):
        '''put last sentence into textinput'''
        self.displayLabel.text = self.last_text 

    def empty(self):
        '''empty textinput'''
        self.displayLabel.text = ''
        self.last_text = ''
        