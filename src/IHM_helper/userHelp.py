
'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : November 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : HMI user help -> HMI manuel 
                @&&&&              #               
                   @&&             # 
'''

from kivy.config import Config
Config.set('kivy', 'keyboard_mode', 'system')

# It’s required that the base Class
# of your App inherits from the App class.
from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty, StringProperty 
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen
from kivy import require
import screeninfo
require("1.8.0")
Window.softinput_mode = ""

import src.wrapper as wrapper 

import config  
Builder.load_file(config.base_path + '/src/IHM_helper/userHelp.kv')

# This class stores the info of .kv file
# when it is called goes to my.kv file
class appHelper_userHelp(Screen):
    active_predict4All = ObjectProperty()
    active_nextWord    = ObjectProperty()
    active_enter       = ObjectProperty()
    active_voice       = ObjectProperty()
    font_size          = NumericProperty()
    nb_pred            = NumericProperty()
    cursor_help        = NumericProperty()
    img_help = StringProperty()
    label_help = StringProperty()
    plot_nb_help = StringProperty()
    
    def __init__(self, **kwargs):
        """init kivi app """
        self.button_color = (193/255,211/255,254/255,127/255)
        super(appHelper_userHelp, self).__init__(**kwargs)
        
        self.cursor_help        = 0
        self.list_img           = [config.base_path + '/src/IHM_helper/img/help/app_usage_0.png', 
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_1.png',
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_2.png', 
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_3.png',
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_4.png', 
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_5.png',
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_6.png', 
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_7.png',
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_8.png',
                                   config.base_path + '/src/IHM_helper/img/help/app_usage_9.png',
            
                                   config.base_path + '/src/IHM_helper/img/help/app_settings_0.png', 
                                   config.base_path + '/src/IHM_helper/img/help/app_settings_1.png',
                                   config.base_path + '/src/IHM_helper/img/help/app_settings_2.png', 
                                   config.base_path + '/src/IHM_helper/img/help/app_settings_3.png',
                                   config.base_path + '/src/IHM_helper/img/help/app_settings_4.png', 
                                   config.base_path + '/src/IHM_helper/img/help/app_settings_5.png', 
                                   
                                   config.base_path + '/src/IHM_helper/img/help/device_settings_0.png', 
                                   config.base_path + '/src/IHM_helper/img/help/device_settings_1.png',
                                   config.base_path + '/src/IHM_helper/img/help/device_settings_2.png', 
                                   config.base_path + '/src/IHM_helper/img/help/device_settings_3.png',
                                   config.base_path + '/src/IHM_helper/img/help/device_settings_4.png', 
                                   config.base_path + '/src/IHM_helper/img/help/device_settings_5.png', 
                                   
                                   config.base_path + '/src/IHM_helper/img/help/aboutus_0.png',
                                   config.base_path + '/src/IHM_helper/img/help/aboutus_1.png',]
        
        self.list_help          = ['Taper du texte dans le champ prévu pour',
                                   'Les boutons sont mis à jour lors de l\'écriture pour \n - Corriger l\'orthographe, la ponctuation\n - Prédire les mots suivants',
                                   'Les applications de votre ordinateur sont listées\nUn clic envoie le texte tapé dans l\'application ciblée',
                                   ' - Remettre la dernière phrase tapée dans le champ de texte \n - Supprimer le contenu du champ de texte',
                                   'Mettre à jour la liste des applications en cours d\'exécution sur l\'ordinateur',
                                   'Activer la dictée vocale \n - Appuyer sur le bouton et dicter la phrase que vous souhaitez écrire\n - L\'application essaie d\'écrire ce que vous avez dicté',
                                   'Aller dans les réglages de l\'application',
                                   'Aller dans les réglages des périphériques',
                                   'Selectionner, ajouter ou supprimer un profil utilisateur',
                                   'Afficher l\'aide et les informations du logiciel',

                                   'Activer ou désactiver Predict 4 All qui corrige l\'orthographe et prédit la suite',
                                   'Activer ou désactiver la touche \'entrée\' automatique \n - pour appuyer sur la touche \'entrée\' à la suite de l\'écriture dans une application', 
                                    'Activer ou désactiver la génération de l\'audio \n - Chaque lettre tapée ou mot dicté edt lu à voix haute par l\'ordinateur', 
                                    'Choisir la taille de la police dans toute l\'application', 
                                    'Choisir le nombre de prédictions effectuées par Predict 4 All', 
                                    'Annuler ou Sauvegarder les modifications', 
                                    
                                    'Sélectionner votre périphérique dans le menu déroulant',
                                    'Sélectionner la méthode de communication du périphérique',
                                    'Identifier votre périphérique \n - Le port dans le cas USB\n - Le nom dans le cas bluetooth',
                                    'Choisir la sensibilité de votre périphérique',
                                    'Commencer la calibration de votre périphérique',
                                    'Annuler ou Sauvegarder les modifications',

                                   'A propos de LR Technologies et du logiciel', 
                                   'Lien vers le code source de l\'application', ]
        
        self.list_size = [[0.55, 0.35], [0.55, 0.35], [0.55, 0.35], [0.55, 0.35], [0.55, 0.35], [0.55, 0.35], [0.55, 0.35], [0.55, 0.35], [0.55, 0.35], [0.55, 0.35],
                          [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], 
                          [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], [0.3, 0.6], 
                          [0.3, 0.35], [0.3, 0.35],]

        self.nb_help   = len(self.list_img)
        self.set_label()
        self.set_img()
        
        
    def resize(self):
        monitor_size, monitor_pos = self.get_monitor_size()    
        Window.size = (monitor_size[0] * self.list_size[self.cursor_help][0], 
                       monitor_size[1] * self.list_size[self.cursor_help][1])
        
        wrapper.main_app.IHM_helper.title = 'Koï rect me - {} > Aide'.format(wrapper.main_app.profil.user_name)
        
    def get_monitor_size(self):
        """get monitor size """
        for m in screeninfo.get_monitors():
            if m.is_primary is True:
                scr_width  = m.width
                scr_height = m.height
                scr_posX   = m.x
                scr_posY   = m.y
        return (scr_width, scr_height), (scr_posX, scr_posY)
    
    def set_label(self):
        self.label_help = self.list_help[self.cursor_help]
        self.plot_nb_help = 'Aide {} / {}'.format(self.cursor_help, self.nb_help)
        
    def set_img(self):
        self.img_help = self.list_img[self.cursor_help]
    
