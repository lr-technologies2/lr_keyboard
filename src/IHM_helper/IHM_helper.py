
'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : November 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-0.9.7 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : HMI helper -> HMI for screens instanciation 
                @&&&&              #               
                   @&&             # 
'''

from kivy.config import Config
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

# It’s required that the base Class
# of your App inherits from the App class.
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager
from kivy import require
import kivy 
require("1.8.0")
Window.softinput_mode = ""

from src.IHM_helper.physicalKeyboard import * 
from src.IHM_helper.userProfil import * 
from src.IHM_helper.userHelp import * 
from src.IHM_helper.deviceProfil import * 
import os 

def install_is_done(file_install):
    return os.path.exists(file_install)  

from kivy.animation import Animation
from kivy.uix.image import Image
from kivy.uix.label import Label
import webbrowser

class mySplashScreen(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.img = Image(source=config.base_path + "/src/IHM_helper/img/splash.png")
        self.label = Label(text='Bienvenue sur Koï rect me ! \n\nLR Technologies - R&D', font_size=30, color=(0, 0, 0, 1))
        box_layout = BoxLayout()
        box_layout.orientation = 'horizontal'
        self.add_widget(box_layout)
        box_layout.add_widget(self.img)
        box_layout.add_widget(self.label)

    def on_enter(self):
        self.enter = True 
        Clock.schedule_once(self.leave_splash, 4)
        self.img.opacity = 0
        self.label.opacity = 0
        animation = Animation(duration=4, opacity=1) 
        animation.start(self.img)
        animation.start(self.label)
    
    def on_waiting(self):
        self.enter = False 
        Clock.schedule_once(self.leave_splash, 4)
        self.img.opacity = 1
        self.label.opacity = 1
        animation = Animation(duration=4, opacity=0) 
        animation.start(self.img)
        animation.start(self.label)

    def leave_splash(self, dt):
        if config.OS == 'Windows':
            if not install_is_done(config.base_path + '/src/utils/.install.lock'): 
                if not self.enter:
                    self.on_enter()
                else:
                    self.on_waiting()
            else:
                wrapper.main_app.java_proc = wrapper.launch_javaConnector()
                wrapper.main_app.IHM_helper.sm.current = 'appHelper_physicalKeyboard'
                wrapper.main_app.IHM_helper.sm.get_screen('appHelper_physicalKeyboard').resize()
        else:
            wrapper.main_app.java_proc = wrapper.launch_javaConnector()
            wrapper.main_app.IHM_helper.sm.current = 'appHelper_physicalKeyboard'
            wrapper.main_app.IHM_helper.sm.get_screen('appHelper_physicalKeyboard').resize()



class AboutUs(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.aboutUs = '[b]Koï rect me ! [/b]\nversion : 1.0.0\nLicence [b]MIT[/b] open source\n\n[b]LR Technologies[/b]\n5, av Albert Durant. \n31700. Blagnac. France\n01 30 97 02 90\n\nRecherche et Développement'
        self.img = Image(source=config.base_path + "/src/IHM_helper/img/splash.png", size_hint=(0.6, 0.6))
        self.label = Label(text=self.aboutUs, markup=True, font_size=20, color=(0, 0, 0, 1))
        layout = BoxLayout(orientation='vertical')
        box_layout = BoxLayout(orientation='horizontal', size_hint=(1, 0.8))
        box_layout.add_widget(self.img)
        box_layout.add_widget(self.label)
        layout.add_widget(box_layout)

        quit = Button(text='OK',
                      color=(0, 0, 0, 1),
                      background_color=(193/255,211/255,254/255,127/255))
        
        git = Button(text='gitlab',
                      color=(0, 0, 0, 1),
                      background_color=(193/255,211/255,254/255,127/255))
        
        
        
        box_button = BoxLayout(orientation='horizontal', size_hint=(1, 0.2))
        quit.bind(on_release=self.quit)
        git.bind(on_release=self.git)
        box_button.add_widget(quit)
        box_button.add_widget(git)
        layout.add_widget(box_button)
        self.add_widget(layout)

    def quit(self, *args):
        wrapper.main_app.IHM_helper.sm.current = 'appHelper_physicalKeyboard'
        wrapper.main_app.IHM_helper.sm.get_screen('appHelper_physicalKeyboard').resize()

    def git(self, *args):
        webbrowser.open('https://gitlab.com/lr-technologies2/lr_keyboard')

    def resize(self):
        monitor_size, monitor_pos = self.get_monitor_size()    
        Window.size = (monitor_size[0]*0.25, monitor_size[1] * 0.3)
        wrapper.main_app.IHM_helper.title = 'Koï rect me - {} > A propos de nous'.format(wrapper.main_app.profil.user_name)
        
    def get_monitor_size(self):
        """get monitor size """
        for m in screeninfo.get_monitors():
            if m.is_primary is True:
                scr_width  = m.width
                scr_height = m.height
                scr_posX   = m.x
                scr_posY   = m.y
        return (scr_width, scr_height), (scr_posX, scr_posY)

# we are defining the Base Class of our Kivy App
class typeHelperApp(App):
    #icon = './src/IHM_helper/logo.png'
    sm = None  # The root screen manager
    def build(self):
        Window.clearcolor = (215/255,227/255,252/255,210/255)
        self.title = 'Koï rect me - {}'.format(wrapper.main_app.profil.user_name)
        self.sm = ScreenManager(transition = kivy.uix.screenmanager.SwapTransition())
        self.sm.add_widget(mySplashScreen(name="mySplashScreen"))
        self.sm.add_widget(AboutUs(name="AboutUs"))
        self.sm.add_widget(appHelper_physicalKeyboard(name="appHelper_physicalKeyboard"))
        self.sm.add_widget(appHelper_userProfil(name="appHelper_userProfil"))
        self.sm.add_widget(appHelper_userHelp(name="appHelper_userHelp"))
        self.sm.add_widget(appHelper_deviceProfil(name="appHelper_deviceProfil"))
        self.sm.current = "mySplashScreen"
        self.sm.get_screen('mySplashScreen')
        return self.sm
    
    def on_start(self):
        self.file_install = config.base_path + '/src/utils/.install.lock'
        if config.OS == 'Windows':
          if not install_is_done(self.file_install):
              self.ask_driver_install()

    def ask_driver_install(self):
        content_save = Button(text='Installer', size_hint=(1, 1))
        content_cancel = Button(text='Annuler', size_hint=(1, 1))
        layout_btn = BoxLayout(orientation="horizontal")
        layout_btn.add_widget(content_cancel)
        layout_btn.add_widget(content_save)

        self.popup_driver = Popup(
            title="Voulez vous installer les drivers ?",
            content=layout_btn, 
            size_hint=(None, None), size=(300, 150),
            auto_dismiss = False, 
            background_color = (0, 0, 0, 1)
        )
        content_cancel.bind(on_release=self.ask_next_install)
        content_save.bind(on_release=self.install_driver)
        
        self.popup_driver.open()

    def ask_next_install(self, *args):
        self.popup_driver.dismiss()
        self.ask_java_install()

    def install_driver(self, *args):
        os.system('tar -xf drivers.zip')
        os.system('start /b .\pololu-cp2102-windows\pololu-cp2102-setup-x64.exe')
        self.ask_next_install()

    
    def ask_java_install(self):
        content_save = Button(text='Installer', size_hint=(1, 1))
        content_cancel = Button(text='Annuler', size_hint=(1, 1))
        layout_btn = BoxLayout(orientation="horizontal")
        layout_btn.add_widget(content_cancel)
        layout_btn.add_widget(content_save)

        self.popup_java = Popup(
            title="Voulez vous installer java ?",
            content=layout_btn, 
            size_hint=(None, None), size=(300, 150),
            auto_dismiss = False, 
            background_color = (0, 0, 0, 1)
        )
        content_cancel.bind(on_release=self.finish_install)
        content_save.bind(on_release=self.install_java)
        
        self.popup_java.open()

    def finish_install(self, *args):
        self.popup_java.dismiss()
        
        f = open(self.file_install, 'w')
        f.write('installed')

    def install_java(self, *args):
        os.system('start /b jdk-21_windows-x64_bin.exe')
        self.finish_install()


    

