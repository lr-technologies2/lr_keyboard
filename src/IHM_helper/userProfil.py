
'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : November 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : HMI user profil -> HMI for user profil settings  
                @&&&&              #               
                   @&&             # 
'''

from kivy.config import Config
Config.set('kivy', 'keyboard_mode', 'system')

# It’s required that the base Class
# of your App inherits from the App class.
from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty 
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen
from kivy import require
import screeninfo
require("1.8.0")
Window.softinput_mode = ""

import src.wrapper as wrapper 

import config  
Builder.load_file(config.base_path + '/src/IHM_helper/userProfil.kv')

# This class stores the info of .kv file
# when it is called goes to my.kv file
class appHelper_userProfil(Screen):
    active_predict4All = ObjectProperty()
    active_nextWord    = ObjectProperty()
    active_enter       = ObjectProperty()
    active_voice       = ObjectProperty()
    font_size          = NumericProperty()
    nb_pred            = NumericProperty()
    
    def __init__(self, **kwargs):
        """init kivi app """
        self.button_color = (193/255,211/255,254/255,127/255)
        self.load_profil()
        super(appHelper_userProfil, self).__init__(**kwargs)
        
    def resize(self):
        monitor_size, monitor_pos = self.get_monitor_size()    
        Window.size = (monitor_size[0]*0.3, monitor_size[1] * 0.4)
        
        self.load_profil()
        wrapper.main_app.IHM_helper.title = 'Koï rect me - {} > Réglages du profil'.format(wrapper.main_app.profil.user_name)
        
    def get_monitor_size(self):
        """get monitor size """
        for m in screeninfo.get_monitors():
            if m.is_primary is True:
                scr_width  = m.width
                scr_height = m.height
                scr_posX   = m.x
                scr_posY   = m.y
        return (scr_width, scr_height), (scr_posX, scr_posY)
    
    def load_profil(self):
        wrapper.main_app.profil.load_profil() 
        profil                  = wrapper.main_app.profil.profil[wrapper.main_app.profil.user_name]
        self.active_predict4All = profil['active_predict4All']
        self.active_nextWord    = profil['active_nextWord']
        self.active_enter       = profil['active_enter']
        self.active_voice       = profil['active_voice']
        self.font_size          = profil['font_size']
        self.nb_pred            = profil['nb_pred']
        self.USB_name           = profil['USB_name']
        self.bluetooth_name     = profil['bluetooth_name']
        self.cursor_speed       = profil['cursor_speed']
        self.active_USB         = profil['active_USB']
        self.active_bluetooth   = profil['active_bluetooth']
        self.device_used_name   = profil['device_used_name']

        wrapper.main_app.logger.logger.debug("[appHelper_physicalKeyboard] - profil loaded -> {}".format(profil))
    
    def save_profil(self):
        '''save profil into json '''
        json                       = wrapper.main_app.profil.profil
        json[wrapper.main_app.profil.user_name]['active_predict4All'] = self.active_predict4All
        json[wrapper.main_app.profil.user_name]['active_nextWord']    = self.active_nextWord
        json[wrapper.main_app.profil.user_name]['active_enter']       = self.active_enter
        json[wrapper.main_app.profil.user_name]['active_voice']       = self.active_voice
        json[wrapper.main_app.profil.user_name]['font_size']          = self.font_size
        json[wrapper.main_app.profil.user_name]['nb_pred']            = self.nb_pred
        json[wrapper.main_app.profil.user_name]['USB_name']           = self.USB_name
        json[wrapper.main_app.profil.user_name]['bluetooth_name']     = self.bluetooth_name
        json[wrapper.main_app.profil.user_name]['cursor_speed']       = self.cursor_speed
        json[wrapper.main_app.profil.user_name]['active_USB']         = self.active_USB
        json[wrapper.main_app.profil.user_name]['active_bluetooth']   = self.active_bluetooth
        json[wrapper.main_app.profil.user_name]['device_used_name']   = self.device_used_name
        
        wrapper.main_app.logger.logger.debug("[appHelper_physicalKeyboard] - profil saved -> {}".format(json))
        wrapper.main_app.profil.save_profil(json)
        
    def checkbox_predict4All(self, instance, value):
        """checkbox to active / deactive next word prediction"""
        self.active_predict4All = value
        
    def checkbox_nextWord(self, instance, value):
        """checkbox to active / deactive next word prediction"""
        self.active_nextWord = value
        
    def checkbox_enter(self, instance, value):
        """checkbox to active / deactive automatic enter press"""
        self.active_enter = value
        
    def checkbox_voice(self, instance, value):
        """checkbox to active / deactive automatic enter press"""
        self.active_voice = value

