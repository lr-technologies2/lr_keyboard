
'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : November 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : device Profil -> HMI for devices profil 
                @&&&&              #               
                   @&&             # 
'''

from kivy.config import Config
Config.set('kivy', 'keyboard_mode', 'system')

# It’s required that the base Class
# of your App inherits from the App class.
from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty, StringProperty 
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen
from kivy import require
import screeninfo
require("1.8.0")
from kivy.uix.gridlayout import GridLayout 
from kivy.uix.popup import Popup 
from kivy.uix.progressbar import ProgressBar
from kivy.clock import Clock
Window.softinput_mode = ""
import src.wrapper as wrapper 
import threading 
import json 

import config  
Builder.load_file(config.base_path + '/src/IHM_helper/deviceProfil.kv')
    

# This class stores the info of .kv file
# when it is called goes to my.kv file
class appHelper_deviceProfil(Screen):
    active_predict4All = ObjectProperty()
    active_nextWord    = ObjectProperty()
    active_enter       = ObjectProperty()
    active_voice       = ObjectProperty()
    font_size          = NumericProperty()
    nb_pred            = NumericProperty()
    USB_name           = StringProperty()
    bluetooth_name     = StringProperty()
    cursor_speed       = NumericProperty()
    active_USB         = ObjectProperty()
    active_bluetooth   = ObjectProperty()
    progress_bar = ObjectProperty()
    
    def __init__(self, **kwargs):
        """init kivi app """
        self.button_color = (193/255,211/255,254/255,127/255)
        with open(config.base_path + '/src/utils/available_devices.json', 'r') as j:
            tmp = j.read()
        devices = json.loads(tmp)
        self.available_devices = devices['devices']
        self.load_profil()
        super(appHelper_deviceProfil, self).__init__(**kwargs)
        
    def resize(self):
        monitor_size, monitor_pos = self.get_monitor_size()    
        Window.size = (monitor_size[0]*0.3, monitor_size[1] * 0.5)
        
        self.load_profil()
        wrapper.main_app.IHM_helper.title = 'Koï rect me - {} > Réglages des périphériques'.format(wrapper.main_app.profil.user_name)
        
    def get_monitor_size(self):
        """get monitor size """
        for m in screeninfo.get_monitors():
            if m.is_primary is True:
                scr_width  = m.width
                scr_height = m.height
                scr_posX   = m.x
                scr_posY   = m.y
        return (scr_width, scr_height), (scr_posX, scr_posY)
    
    def load_profil(self):
        wrapper.main_app.profil.load_profil()
        profil                  = wrapper.main_app.profil.profil['Utilisateur']
        self.active_predict4All = profil['active_predict4All']
        self.active_nextWord    = profil['active_nextWord']
        self.active_enter       = profil['active_enter']
        self.active_voice       = profil['active_voice']
        self.font_size          = profil['font_size']
        self.nb_pred            = profil['nb_pred']
        self.USB_name           = profil['USB_name']
        self.bluetooth_name     = profil['bluetooth_name']
        self.cursor_speed       = profil['cursor_speed']
        self.active_USB         = profil['active_USB']
        self.active_bluetooth   = profil['active_bluetooth']
        self.device_used_name   = profil['device_used_name']

        self.update_device()
        wrapper.main_app.logger.logger.debug("[appHelper_deviceProfil] - profil loaded -> {}".format(profil))
    
    def save_profil(self):
        '''save profil into json '''
        USB_name                   = self.ids.USB_name_display.text
        bluetooth_name             = self.ids.bluetooth_name_display.text
        self.active_device = not self.active_devices[0]
        json                       = wrapper.main_app.profil.profil
        json[wrapper.main_app.profil.user_name]['active_predict4All'] = self.active_predict4All
        json[wrapper.main_app.profil.user_name]['active_nextWord']    = self.active_nextWord
        json[wrapper.main_app.profil.user_name]['active_enter']       = self.active_enter
        json[wrapper.main_app.profil.user_name]['active_voice']       = self.active_voice
        json[wrapper.main_app.profil.user_name]['font_size']          = self.font_size
        json[wrapper.main_app.profil.user_name]['nb_pred']            = self.nb_pred
        json[wrapper.main_app.profil.user_name]['USB_name']           = USB_name
        json[wrapper.main_app.profil.user_name]['bluetooth_name']     = bluetooth_name
        json[wrapper.main_app.profil.user_name]['cursor_speed']       = self.cursor_speed
        json[wrapper.main_app.profil.user_name]['active_USB']         = self.active_USB
        json[wrapper.main_app.profil.user_name]['active_bluetooth']   = self.active_bluetooth
        json[wrapper.main_app.profil.user_name]['device_used_name'] = self.device_used_name
        
        wrapper.main_app.logger.logger.debug("[appHelper_deviceProfil] - profil saved -> {}".format(json))
        wrapper.main_app.profil.save_profil(json)
        wrapper.main_app.deviceESP.destroy_device()
        if(self.active_device):
            wrapper.main_app.deviceESP.create_device()
            wrapper.main_app.deviceESP.start_device()
        
    def update_device(self):
        self.active_devices = [False for i in range(len(self.available_devices))]
        for i in range(len(self.available_devices)):
            if self.device_used_name == self.available_devices[i]:
                self.active_devices[i] = True 
        
    
    def update_device_spinner(self):
        wrapper.main_app.logger.logger.debug("[deviceProfil] - selected device : {}".format(self.ids.spinner.text))
        self.device_used_name = self.ids.spinner.text
        self.update_device()

    def checkbox_active_device(self, instance, value):
        """checkbox to active device"""
        self.active_device = value

    def checkbox_USB_device(self, instance, value):
        """checkbox to select USB device"""
        self.active_USB = value
                
    def checkbox_bluetooth_device(self, instance, value):
        """checkbox to select USB device"""
        self.active_bluetooth = value
        
    def radiobutton_mode_device(self):
        """plot selection"""
        wrapper.main_app.logger.logger.debug("[deviceProfil] - bluetooth = {}; USB = {}".format(self.active_bluetooth, self.active_USB))
        
    def update_cursor_speed(self):
        pass
    
    def calibration_button(self):
        """ Called when calibration button is hit on the GUI
        """
        Clock.schedule_once(self.calib, 0)

    def calib(self, dt):
        """Start device calibration scenario"""
        if(wrapper.main_app.deviceESP is None):
            wrapper.main_app.logger.logger.debug("[deviceESP] - No device connected to the application: calibration impossible")
            return -1
        self.calibrate_window_open()
        t = threading.Thread(target = wrapper.main_app.deviceESP.calibrate)
        t.start()
        
    def calibrate_window_open(self):
        """ Open the calibration popup with the default instruction.
            Also starts livy threads to update the popup
        """
        self.progress_bar = CircularProgressBar()
        layout = GridLayout(cols = 1, padding = 10)
        layout.add_widget(self.progress_bar)
        self.popup = Popup(
            title="Initialising calibration",
            content=layout, 
            size_hint=(None, None), size=(270, 300),
            auto_dismiss = False
        )
        self.popup.bind(on_open=self.calibration_check_init)
        self.popup.open()

    def calibration_check_init(self, instance):
        """Start calibration check thread
        
        Args:
            instance (_type_): Kivy object (not to pass)
        """
        self.clock = Clock.schedule_interval(self.calibration_check, 0.025)

    def calibration_check(self,dt):
        """ Checks at fixed intervals the calibration status

        Args:
            dt (int): kivy tempo
        """
        if self.progress_bar.value < 100:
            self.progress_bar.set_value(self.progress_bar.value + 1)
        else:
            self.progress_bar.set_value(0)
        if not wrapper.main_app.deviceESP.is_doing_calibration:           
            self.clock.cancel()
            self.popup.dismiss()
        if(wrapper.main_app.deviceESP.current_calibration_instruction != self.popup.title):
            self.progress_bar.set_value(0)
            self.popup.title = wrapper.main_app.deviceESP.current_calibration_instruction
    
    def save_calibrate(self):
        pass 

        

from kivy.graphics import Color, Ellipse, Rectangle
from kivy.core.text import Label as CoreLabel
class CircularProgressBar(ProgressBar):

    def __init__(self, **kwargs):
        super(CircularProgressBar, self).__init__(**kwargs)
        # Set constant for the bar thickness
        self.thickness = 30
        self.fill = True 
        # Create a direct text representation
        self.label = CoreLabel(text="0%", font_size=self.thickness)
        # Initialise the texture_size variable
        self.texture_size = None
        # Refresh the text
        self.refresh_text()
        # Redraw on innit
        self.draw()

    def draw(self):
        with self.canvas:
            # Empty canvas instructions
            self.canvas.clear()
            # Draw no-progress circle
            if not self.fill:
                Color(0.36, 0.77, 1)
            else:
                Color(0.26, 0.26, 0.26)
            Ellipse(pos=self.pos, size=self.size)

            # Draw progress circle, small hack if there is no progress (angle_end = 0 results in full progress)
            if self.fill:
                Color(0.36, 0.77, 1)
            else:
                Color(0.26, 0.26, 0.26)
            Ellipse(pos=self.pos, size=self.size,
                    angle_end=(0.001 if self.value_normalized == 0 else self.value_normalized*360))
            # Draw the inner circle (colour should be equal to the background)
            Color(0.1, 0.1, 0.1)
            Ellipse(pos=(self.pos[0] + self.thickness / 2, self.pos[1] + self.thickness / 2),
                    size=(self.size[0] - self.thickness, self.size[1] - self.thickness))
            # Center and draw the progress text
            Color(1, 1, 1, 1)
            #added pos[0]and pos[1] for centralizing label text whenever pos_hint is set
            Rectangle(texture=self.label.texture, size=self.texture_size,
                  pos=(self.size[0] / 2 - self.texture_size[0] / 2 + self.pos[0], self.size[1] / 2 - self.texture_size[1] / 2 + self.pos[1]))


    def refresh_text(self):
        # Render the label
        self.label.refresh()
        # Set the texture size each refresh
        self.texture_size = list(self.label.texture.size)

    def set_value(self, value):
        # Update the progress bar value
        self.value = value
        if self.value == 0:
            self.fill = not self.fill 
        # Update textual value and refresh the texture
        dot = ''
        for i in range(int(self.value)):
            if i%20==0:
                dot += '.'
        for i in range(int(self.value), 100):
            if i%20==0:
                dot += ' '
        txt = 'En cours ' + dot 
        self.label.text = txt #str(int(self.value_normalized*100)) + "%"
        self.refresh_text()
        # Draw all the elements
        self.draw()