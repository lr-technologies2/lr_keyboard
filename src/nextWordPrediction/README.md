# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : nextWordPrediction   

#### Objectifs 

Prédire le mot suivant à partir des dernières phrases saisies par l'utilisateur  

#### Usages 

* Les dernières phrases saisies sont stocké dans le fichier `./src/nextWordPrediction/data/dataset.txt`  
* La fonction `fill_lexicon()` remplit le lexique de tout les mots présent dans les phrases précédentes  
* La fonction `compute_probabilities()` calcul les probabilités des mots suivants  
* Le dataset contient les 1000 dernières phrases  

```
# Préparer le lexique  
wrapper.main_app.nextWordPrediction.fill_lexicon()
wrapper.main_app.nextWordPrediction.compute_probabilities() 

# prédire le mot suivant  
wrapper.main_app.nextWordPrediction.predict("Bonjour") 

# augmenter le dataset  
wrapper.main_app.nextWordPrediction.add_sentence_to_lexical(str)
```

#### Particularités 

* La taille du dataset reste à définir pour que les prédictions restent instantanées  
* Actuellement le dataset n'est pas mis à jours lors de l'utilisation de l'application  
> Certaines phrases saisies par l'utilisateur ne peuvent être sauvegardée (mot de passe, identifiant, numéro de compte bancaire)  
>> Filtrer les phrases qui augmentent le dataset ?  
>> Crypter le dataset ?  
