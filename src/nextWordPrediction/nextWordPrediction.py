#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : May 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - June 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : next word prediction -> next word prediction component 
                @&&&&              #               
                   @&&             # 
'''

# import modules
import numpy as np

import src.wrapper as wrapper 

class nextWordPrediction:
    """class nextWordPrediction"""
    def __init__(self, nb_words):
        """init of nextWordPrediction"""
        self.data_path = './src/nextWordPrediction/data/dataset.txt'
        """path to dataset"""
        self.lexicon = {}
        """lexicon : dict with example -> lexicon[word] = (nextword, proba)"""
        self.nb_words = nb_words 
        """nb words to predict"""
        self.line_max = 1000
        """dataset limit to 1000 lines"""

    def update_lexicon(self, current : str, next_word : str) -> None:
        """update lexicon : create dict with example -> lexicon[word] = (nextword, proba) for one word"""
        # Add the input word to the lexicon if it in there yet.
        if current not in self.lexicon:
            self.lexicon.update({current: {next_word: 1} })
            return

        # Recieve te probabilties of the input word.
        options = self.lexicon[current]

        # Check if the output word is in the propability list.
        if next_word not in options:
            options.update({next_word : 1})
        else:
            options.update({next_word : options[next_word] + 1})

        # Update the lexicon
        self.lexicon[current] = options
        
    def fill_lexicon(self):
        """fill lexicon with dataset words """
        # Populate lexicon
        with open(self.data_path, 'r') as dataset:
            for line in dataset:
                words = line.strip().split(' ')
                for i in range(len(words) - 1):
                    self.update_lexicon(words[i], words[i+1])
                    
    def compute_probabilities(self):
        """Compute probabilites of each next word"""
        # Adjust propability
        for word, transition in self.lexicon.items():
            transition = dict((key, value / sum(transition.values())) for key, value in transition.items())
            self.lexicon[word] = transition
            
    def add_sentence_to_lexical(self, sentence):
        """add a sentence to the file dataset.txt """
        f = open(self.data_path, 'r')
        txt = f.read()
        f.close()
        lines = txt.split('\n')
        del lines[-1]
        f = open(self.data_path, 'w')
        if len(lines) > self.line_max:
            lines = lines[-self.line_max:]
        for line in lines:
            f.write(line + '\n')
        f.write(sentence + '\n')
        f.close
            
            
            


    def predict(self, input):
        """predict self.nb_words next words after input"""
        # init ret as list of empty string 
        ret = ['' for i in range(self.nb_words)]
        # if input exist in lexicon 
        if input in self.lexicon:
            # get options for this input 
            options = self.lexicon[input]
            # cast as list 
            words = list(options.keys())
            probas = list(options.values())
            # how many words can we predict ? 
            nb_max = np.min([len(words), self.nb_words])
            # for each word to predict 
            for i in range(nb_max):
                # find best probability 
                idx = np.argmax(probas)
                # save this prediction
                ret[i] = words[idx]
                # del word from temporary list 
                del probas[idx]
                del words[idx]
            wrapper.main_app.logger.logger.debug("[nextWordPrediction] - found {} in lexicon and predict {}".format(input, ret))
        else:
            wrapper.main_app.logger.logger.debug("[nextWordPrediction] - unable to find {} in lexicon".format(input))
        return ret 

