#!/usr/bin/env python3
# coding: utf-8

'''
word prediction component (not used)

Licence    : MIT
Author     : Edouard Villain (evillain@lrtechnologies.fr) - LR Technologies
Created    : April 2023
Last update: Edouard Villain - April 2023
version    : v-1.0.0
'''

"""module wordPrediction"""

# import modules
import numpy as np
import re
import pandas as pd

import src.wrapper as wrapper 

class wordPrediction():
    """class wordPrediction : try to finish your current word """
    def __init__(self, nb_words):
        """init of wordPrediction"""
        np.seterr(invalid='ignore')
        self.filepath = "./src/wordPrediction/data/"
        """filepath to <letter>.txt """
        self.wordfilepath = "./src/wordPrediction/data/liste_de_frequence_mots.ods"
        """filepath to word frequency """
        self.nb_words = nb_words 
        """Number of word to search"""

    def predict(self, input):
        """Predict word knowing most likely characters
        input: Word from the buffer
        Returns:
        list: list of predicted words
        """
        # read file with words frequency 
        regex = r'' + input + '.+'
        wrapper.main_app.logger.logger.debug("[wordPrediction] - regex used : {}".format(regex))
        excelOutput = pd.read_excel(self.wordfilepath, engine="odf")
        wordFrequenciesDict = excelOutput.to_dict(orient ='records')
        wordsDict = {}
        for elem in wordFrequenciesDict:
            # if current elem match input 
            if re.match(regex ,elem["Mot"]):
                # add elem to dict 
                wrapper.main_app.logger.logger.debug("[wordPrediction] - new word found : {}".format(elem["Mot"]))
                wordsDict[elem["Mot"]]= elem["Fréquence"]

        # sort word by frequency 
        listWordsSorted = []
        for k, v in sorted(wordsDict.items(), key=lambda x: x[1], reverse = True):
            listWordsSorted.append(k)
        # return right amount of word 
        return listWordsSorted[0:self.nb_words]
