# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : wordPrediction   

#### Objectifs 

Prédire la lettre suivante  

#### Usages 

* Le données utilisées pour les prédictions sont situées dans le fichier `./src/wordPrediction/data/liste_de_frequence_mots.ods`  
* La fonction `predict()` prédit la lettre suivante   

```
# Préparer le lexique  
wrapper.main_app.wordPrediction.predict("Bonj")
```

#### Particularités 

* Le composant n'est pas utilisé dans le projet  
> La complexité de l'algorithme de prédiction ne permet pas de faire du temps réél  

