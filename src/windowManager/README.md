# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap.  

## Composant : windowManager    

#### Objectifs 

Gestion des fenêtres / applications en cours d'exécution sur l'ordinateur, ainsi que de leurs focus  

#### Usages 

* La fonction `update_opened_windows()` accessible depuis le wrapper permet de mettre à jours les fenêtres et applications en cours d'exécution  
* La fonction `set_active_window(nom)` accessible depuis le wrapper permet de focus la fenêtre / application spécifiée par son nom  

```
# mise à jours des fenêtres / applications ouvertes 
wrapper.main_app.windowManager.update_opened_windows()
# mettre le focus sur firefox 
wrapper.main_app.windowManager.set_active_window('firefox')
```

#### Particularités 

* Seul la version Linux est actuellement développée   

