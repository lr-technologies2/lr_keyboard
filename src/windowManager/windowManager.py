#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : May 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - November 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : windows manager -> get opened windows and set focus 
                @&&&&              #               
                   @&&             # 
'''

# import modules
import src.wrapper as wrapper 

import platform 
if platform.system() == 'Windows':
    import win32gui 
    import win32con
else: 
    import gi
    gi.require_version('Wnck', '3.0')
    gi.require_version('Gtk', '3.0')
    from gi.repository import Wnck, GdkX11, Gdk, Gtk 

class windowManager_Linux:
    """class windowManager"""
    def __init__(self):
        """Init windowManager"""
        self.screen = Wnck.Screen.get_default()
        """Wnck screen: get windows"""
        self.screen.force_update()
        self.l_windows = []
        """list of opened windows"""
        self.l_windows_name = []
        """list of opened windows's name"""
        self.default_display = Gdk.Display.get_default()
        self.default_root_xwindow = GdkX11.x11_get_default_root_xwindow()
        self.display = GdkX11.X11Window.lookup_for_display(self.default_display, self.default_root_xwindow)
        """display: used to get x11 server time"""

        
    def update_opened_windows(self):
        """update_opened_windows: update opened windows"""
        self.l_windows_name = []
        # update current screen and get windows 
        now = GdkX11.x11_get_server_time(self.display)
        # finish gtk event 
        while Gtk.events_pending():
            Gtk.main_iteration()
        self.screen.force_update()
        self.l_windows = self.screen.get_windows()
        # get also windows's name 
        for w in self.l_windows:
            self.l_windows_name.append('{} : {} ...'.format(w.get_class_group_name().split('.')[-1], w.get_name()[:25]))
        wrapper.main_app.logger.logger.debug("[windowManager] - Opened windows : {}".format(self.l_windows_name))

        
    def set_active_window(self, name):
        """set_active_window: set focus on windows by matching arg name """
        # iterate list of windows name 
        for i in range(len(self.l_windows_name)):
            # if matching name 
            if name in self.l_windows_name[i]:
                # get time of X11 server 
                now = GdkX11.x11_get_server_time(self.display)
                # finish gtk event 
                while Gtk.events_pending():
                    Gtk.main_iteration()
                # set focus on window
                self.l_windows[i].activate(now)
                wrapper.main_app.logger.logger.debug("[windowManager] - focus set on window : {}".format(self.l_windows_name[i]))
                


class windowManager_Windows:
    """class windowManager"""
    def __init__(self):
        self.l_windows_name = []
        self.hwnd = []
        self.excluded = ['Program Manager']

    def _update_opened_windows(self, hwnd, lParam):
        if win32gui.IsWindowVisible(hwnd):
            tmp_name = win32gui.GetWindowText(hwnd)
            if tmp_name != '':
                if not tmp_name in self.excluded:
                    if not tmp_name in self.l_windows_name:
                        self.l_windows_name.append(tmp_name)
                        self.hwnd.append((hwnd, tmp_name))
        
    def update_opened_windows(self):
        """update_opened_windows: update opened windows"""
        self.l_windows_name = []
        self.hwnd = []
        win32gui.EnumWindows(self._update_opened_windows, None)
        wrapper.main_app.logger.logger.debug("[windowManager] - Opened windows : {}".format(self.l_windows_name))

        
    def _set_active_window(self, hwnd, name ):
        if win32gui.IsWindowVisible( hwnd ):
            a = (hwnd, win32gui.GetWindowText( hwnd ))
            if name in a:
                win32gui.SetForegroundWindow(a[0])

    def set_active_window(self, name):
        """set_active_window: set focus on windows by matching arg name """
        win32gui.EnumWindows( self._set_active_window, name )
        wrapper.main_app.logger.logger.debug("[windowManager] - focus set on window : {}".format(name))
                