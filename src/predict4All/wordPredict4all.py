
'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : April 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - September 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : predict4All -> correction and prediction component 
                @&&&&              #               
                   @&&             # 
'''

from py4j.java_gateway import JavaGateway
import src.wrapper as wrapper 

class wordPredict4all():
    def __init__(self):
        gateway = JavaGateway()
        self.predict4allConnector = gateway.entry_point
        # throws exception if JavaGateway is not available
        #gateway.jvm.System.out.println("Gateway established !")

    def predict(self, input):
        """
        The predict function execute the Predict4allConnector.getPredictionList(String input)
        It gets a result of five word predictions from any string input

        :param self: Access the attributes and methods of the class in python
        :param input: Pass the input to the predict function
        :return: A list of predictions
        """
        ret = self.predict4allConnector.getPredictionCorrectionList(input)
        wrapper.main_app.logger.logger.debug("[wordPredict4all] - trying to predict {} and found {}".format(input, ret))
        return ret


