/*
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : April 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - September 2023
      @@@/(((// @@/*               # version     : v-0.9.7 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : predict4All connector -> java implementation 
                @&&&&              #               
                   @&&             # 
*/

import org.predict4all.nlp.language.LanguageModel;
import org.predict4all.nlp.language.french.FrenchDefaultCorrectionRuleGenerator;
import org.predict4all.nlp.language.french.FrenchLanguageModel;
import org.predict4all.nlp.ngram.dictionary.StaticNGramTrieDictionary;
import org.predict4all.nlp.ngram.dictionary.DynamicNGramDictionary;
import org.predict4all.nlp.prediction.PredictionParameter;
import org.predict4all.nlp.prediction.WordPrediction;
import org.predict4all.nlp.prediction.WordPredictionResult;
import org.predict4all.nlp.prediction.WordPredictor;
import org.predict4all.nlp.words.WordDictionary;
import py4j.GatewayServer;


import org.predict4all.nlp.words.correction.CorrectionRule;
import org.predict4all.nlp.words.correction.CorrectionRuleNode;
import org.predict4all.nlp.words.correction.CorrectionRuleNodeType;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Connecteur Predict4all Python/Java
 * @author Thomas HERBIN
 * @see <a href="https://www.py4j.org/">Py4J</a>
 */

public class Predict4allConnector {
    private static final String FILE_NGRAMS_PATH = "data/fr_ngrams.bin";
    private static final String FILE_WORDS_PATH = "data/fr_words.bin";
    //private final WordPredictor wordPredictor;
    private final WordPredictor WordCorrector; 
    //static Logger logger = Logger.getLogger(Predict4allConnector.class);
    

    //final static Logger logger2 = Logger.getLogger(Predict4allConnector.class);
    //logger2.setLevel(Level.OFF);

    /**
     * Initialisation of WordPredictor
     * @throws IOException if fr_ngrams.bin or fr_words.bin is not found
     */
    public Predict4allConnector() throws IOException {
        LanguageModel languageModel = new FrenchLanguageModel();
        PredictionParameter predictionParameter = new PredictionParameter(languageModel);
        WordDictionary dictionary = WordDictionary.loadDictionary(languageModel, new File(FILE_WORDS_PATH));
        StaticNGramTrieDictionary ngramDictionary = StaticNGramTrieDictionary.open(new File(FILE_NGRAMS_PATH));
        CorrectionRuleNode root = new CorrectionRuleNode(CorrectionRuleNodeType.NODE);
        root.addChild(FrenchDefaultCorrectionRuleGenerator.CorrectionRuleType.WORD_SPACE_APOSTROPHE.generateNodeFor(predictionParameter));
        root.addChild(FrenchDefaultCorrectionRuleGenerator.CorrectionRuleType.PHONEM_CONFUSION_SET.generateNodeFor(predictionParameter));
        root.addChild(FrenchDefaultCorrectionRuleGenerator.CorrectionRuleType.HOMOPHONE.generateNodeFor(predictionParameter));
        root.addChild(FrenchDefaultCorrectionRuleGenerator.CorrectionRuleType.ACCENTS.generateNodeFor(predictionParameter));
        root.addChild(FrenchDefaultCorrectionRuleGenerator.CorrectionRuleType.VISUAL_CONFUSION.generateNodeFor(predictionParameter));
        root.addChild(FrenchDefaultCorrectionRuleGenerator.CorrectionRuleType.HEARING_CONFUSION.generateNodeFor(predictionParameter));
        predictionParameter.setCorrectionRulesRoot(root);
        predictionParameter.setEnableWordCorrection(true);

        this.WordCorrector = new WordPredictor(predictionParameter, dictionary, ngramDictionary);
    }

    /**
     * Method which predict a list of words from an input
     * @param input the input string (can be a sentence)
     * @return a list of five words
     */
    public List<?> getPredictionCorrectionList(String input) {
        try {
            WordPredictionResult predictionResult = WordCorrector.predict(input);

            return predictionResult.getPredictions().stream()
                    .map(WordPrediction::getPredictionToDisplay)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }



    /*
    Application which give access to Predict4all
     */
    public static void main(String[] args) {
        //logger.setLevel(Level.OFF);
        System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "off");
        try {
            Predict4allConnector app = new Predict4allConnector();
            GatewayServer server = new GatewayServer(app);
            server.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
