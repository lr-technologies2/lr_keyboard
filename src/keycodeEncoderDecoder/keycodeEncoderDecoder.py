#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : May 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - October 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : keycode encoder decoder -> decode and encode keycode events 
                @&&&&              #               
                   @&&             # 
'''

# import modules
import src.wrapper as wrapper 

class keycodeEncoderDecoder:
    """class keycodeEncoderDecoder"""
    def __init__(self):
        """init of keycodeEncoderDecoder"""
        self.to_ignore = {} 
        """to_ignore: dict of weird characters"""
        self.upper = {}
        """upper: dict of char when user press shif or capslock"""
        self.ctrl_alt = {}
        """ctrl_alt: dict of char when user press altgr or ctrl + alt"""
        self.non_numeric = {}
        """non_numeric: dict of char under numeric key for keyboard without numeric padd"""
        #if wrapper.main_app.OS == 'Linux':
        self.create_decoder_linux()
        
    def create_decoder_linux(self):
        """create dict to decode keycode"""
        self.to_ignore   = {'İ':'', 'ı':'', 'į':'', 'ĭ':'', 'Ĥ':'', 'ĥ':'', 'ě':'', 
                        'ě':'', 'Ĝ':'', 'ĝ':'', 'Ğ':'', 'ğ':'', 'Ġ':'', 'ġ':'', 
                        'Ģ':'', 'ģ':'', 'Ĵ':'', 'ĳ':''}
        self.upper       = {',':'?', ';':'.', ':':'/', '!':'§', 'ù':'%', '*':'µ', '$':'£', ')':'°' , '=':'+'}
        self.ctrl_alt    = {'2':'~', '3':'#', '4':'{', '5':'[', '6':'|', '7':'`', '8':'\\', '9':'^',
                            '0':'@', ')':']', '=':'}'}
        self.non_numeric = {'1':'&', '2':'é', '3':'"', '4':'\'', '5':'(', '6':'-', '7':'è', 
                            '8':'_', '9':'ç', '0':'à', ')':')', '=':'='}
        self.arrows = [273, 274, 275, 276]
                
    def encode(self, char):
        """encode a char into a keycode or transform char into sequence of pyautogui press : not used

        Args:
        char (str): character to encode
        """
        #if wrapper.main_app.debug:
        #    wrapper.main_app.logger.log("[keycodeEncoderDecoder] - input char {}".format(char))
        # encode your char in keycode... 
    
    def decode(self, keycode): 
        """decode(self, keycode): 
        filter keycode and return if possible corresponding char 
        catch ctrl, alt and shift button 
        catch backspace 

        Args:
        keycode (keycode): example -> (window, 8, 42, 'v', ['ctrl'])

        Returns:
        str: char corresponding to keycode 
        """
        # log 
        wrapper.main_app.logger.logger.debug("[keycodeEncoderDecoder] - input keycode {}".format(keycode))
        
        # if backspace return 'del' 
        if keycode[-2] == None and keycode[-3] == 42 and keycode[-4] == 8:
            return 'del'
        
        # if arraow return '' 
        if keycode[-2] == None and keycode[1] in self.arrows:
            return ''
        
        # if ascii keycode == 1073741824 -> ^ gestion 
        if keycode[1] == 1073741824:
            if 'shift' in keycode[-1] or 'capslock' in keycode[-1]:
                return '¨'
            else:
                return '^'
        # exclude weird caracters that seems to correspond to shift / ctrl / fn / fx press 
        if keycode[-2] in self.to_ignore.keys():
            return self.to_ignore[keycode[-2]]
        
        # now deal with shift / capslock  
        if 'shift' in keycode[-1] or 'capslock' in keycode[-1]:
            # if non alphabetic key 
            if keycode[-2] in self.upper.keys():
                return self.upper[keycode[-2]]
            else:
                # if alphabetic key 
                return keycode[-2].upper()
        
        # now deal with numeric 
        if keycode[-2] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '°', '+', ')', '=']:
            # if user try to reach ctrl + alt or altgr (altgr seems same keycode as alt)
            if ('ctrl' in keycode[-1] and 'alt' in keycode[-1]) or 'alt' in keycode[-1]:
                return self.ctrl_alt[keycode[-2]]
            else:
                return self.non_numeric[keycode[-2]]
        
        return keycode[-2]
