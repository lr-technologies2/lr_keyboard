# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : keycodeEncoderDecoder    

#### Objectifs 

Transformer un événement clavier (keycode) en caractère et inversement  

#### Usages 

La fonction `decode(keycode)` accessible depuis le wrapper permet de retourner le caractère associé au keycode.  

```
char = wrapper.main_app.keycodeEncoderDecoder.decode(keycode)
```

#### Particularités 

* Seul la version Linux est actuellement développée  
* La fonction `encode(char)` existe mais n'est pas implémentée car elle n'est pas utilisée  

