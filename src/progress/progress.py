#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : August 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - August 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : progress -> save user tools usage for static analysis 
                @&&&&              #               
                   @&&             # 
'''

import src.wrapper as wrapper 
from csv import writer
import time 
from datetime import datetime
import config 

class progress:
    '''class progress : save user progress '''
    def __init__(self):
        self.nb_typed_letter     = 0
        self.nb_letter_completed = 0
        self.nb_letter_dictated  = 0
        self.ret_file = config.base_path + '/src/progress/progress.csv'
        self.start               = time.time()
        '''start: starting timer'''
        self.now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        '''now: date at starting app'''
        
    def save(self):
        '''save progress into csv'''
        duration = time.time() - self.start 
        line = [self.now, 
                duration, 
                self.nb_typed_letter, 
                self.nb_letter_completed, 
                self.nb_letter_dictated, 
                self.nb_typed_letter / duration, 
                self.nb_letter_completed / duration, 
                self.nb_letter_dictated / duration]
        
        wrapper.main_app.logger.logger.debug("[progress] - new progress added : {}".format(line))
            
        with open(self.ret_file, 'a') as f_object:
            writer_object = writer(f_object)
            writer_object.writerow(line)
            f_object.close()