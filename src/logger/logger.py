#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : April 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - June 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : logger -> log HMI and components events 
                @&&&&              #               
                   @&&             # 
'''

# import modules
import logging 
import logging.config
import sys 
import config 

class logger:
    """logger class """
    def __init__(self):
        """initialization of logger using logging module"""
        #logging.config.fileConfig(fname=conf, disable_existing_loggers=False)
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s - {%(filename)s:%(funcName)s():%(lineno)d} - %(levelname)s - %(message)s',
            handlers=[
                logging.FileHandler(config.base_path + '/src/utils/LR_keyboard_log.log'),
                logging.StreamHandler()
            ]
        )
        self.logger = logging.getLogger('LR_keyboard')
        logging.getLogger("SLF4J").setLevel(logging.ERROR)
        logging.getLogger("tqdm").setLevel(logging.ERROR)
        logging.getLogger("py4j.java_gateway").setLevel(logging.ERROR)
        logging.getLogger("gtts.tts").setLevel(logging.ERROR)
        logging.getLogger("gtts.lang").setLevel(logging.ERROR)
        logging.getLogger("playsound").setLevel(logging.ERROR)
        logging.getLogger("urllib3.connectionpool").setLevel(logging.ERROR)

    def write(self, message):
        self.logger.debug(message)
    def flush(self):
        self.logger.debug(sys.stderr)