# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : logger 

#### Objectifs 

Ecrire la trace de tout les événements se déroulant lors de l'exécution du programme 

#### Usages 

* Le logger est accessible depuis le wrapper 
* L'appel de la fonction log du logger se fait uniquement en mode debug 

```
if wrapper.main_app.debug:
	wrapper.main_app.logger.log("[COMPOSANT] - something happened : {}".format(something))
```

#### Particularités 

Dans le fichier `./src/logger/logger.py` la fonction `__init__` désactive certains warnings pour conserver un log lisible.   
> Par exemple les log de la porte java python 


