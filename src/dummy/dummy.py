#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : April 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - November 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : Dummy -> example component 
                @&&&&              #               
                   @&&             # 
'''

# import module 
import src.wrapper as wrapper 

class dummy:
    """This is the dummy component"""

    def __init__(self, arg):
        """initialization of dummy object"""
        self.arg = arg
        """int: Docstring of self.arg after the self.arg declaration"""

    def myFunc(self, arg):
        """myFunc compare self. arg and arg
        return boolean """
        wrapper.main_app.logger.logger.debug("[dummy] - je log depuis global")
        if self.arg == arg:
            return True
        else:
            return False
