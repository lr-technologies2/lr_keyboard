# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : dummy 

#### Objectifs 

Exemple d'un composant vide et inutile  

#### Usages 

Le composant dummy n'est pas utilisé et ne sert qu'à montrer comment intégrer un composant dans le projet  

#### Particularités 

> Le composant dummy n'est pas utilisé et est déstiné à être supprimé  

