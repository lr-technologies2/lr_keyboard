# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap.  

## Dossier src

Collecter tout les composants du projet  
Un wrapper pour les instancier tous  

### Composants 

* Un dossier par composant  
> Un composant remplit un objectif, une fonctionalité  
> Un composant possède un nom représentatif  
* Nomenclature d'un composant  
> Un composant nommé nomComposant 
>> est un dossier nommé `nomComposant` situé dans le dossier src  
>> est un fichier `./src/nomComposant/nomComposant.py`   
>>> ce fichier contient une classe `nomComposant`  
>>> l'objet `nomComposant` est instancié dans le wrapper  
* Un composant est documenté  
> En utilisant la docstring pour documenter :
>> le module python  
>> les classes python  
>> les attributs de la classe python  
>> les fonctions de la classe python  
* Un composant est testé  
> le fichier `./test/nomComposant/test_nomComposant.py` existe  
> Les tests couvrent tout les cas d'utilisation du composant  
> 100% des tests doivent être réussi  
> Le composant doit viser 100% de couverture de code  
>> Dans certains cas il n'est pas possible de tester un composant  
>> Mais il est possible de justifier l'absence de tests  

## Wrapper 

Instancier tout les composants nécessaires au fonctionnement du projet  

#### Usages 

* Le wrapper est instancié dans le fichier `main.py`  
* Le wrapper doit être importé dans chaque composant : `import src.wrapper as wrapper`  
* Tout les composants du wrapper sont accessibles via une variable golable : `wrapper.main_app.nomComposant.nomFonction()`  

#### Particularités 

* Le wrapper n'est pas accessible pendant l'instanciation des composants  

Exemple typique ne fonctionnant pas :  

```
class nomComposant:
    '''je suis documenté''' 
    def __init__(self, arg):
        ''je suis aussi documenté''' 
        self.arg = arg 
        '''arg: wow, such argument''' 
        if wrapper.main_app.debug:
            wrapper.main_app.logger.log("[nomComposant] : init arg = {}".format(self.arg))
```

Lorsque `nomComposant` est instancié par le wrapper, la variable globale `main_app` du wrapper n'est pas encore totalement initialisée et les composants ne sont donc pas accessibles  

Solution : 
```
class nomComposant:
    '''je suis documenté''' 
    def __init__(self):
        ''je suis aussi documenté''' 
        self.arg = None 
        '''arg: wow, such argument''' 

    def set_arg(self, arg):
        ''Miracle, moi aussi je suis documenté''' 
        self.arg = arg  
        if wrapper.main_app.debug:
            wrapper.main_app.logger.log("[nomComposant] : init arg = {}".format(self.arg))
```

### Liste des composants  

| Composant   |      Fonctions      | Couverture | 
|:----------|:--------------|:-------------|
| dummy | composant d'exemple mais inutile | 100 % | 
| keycodeEncodeDecoder | Conversion évenement clavier <-> caractère | 100 % | 
| logger | trace des évenements survenus | 100 % | 
| nextWordPrediction | Prédiction du prochain mot à partir des phrases précédente | 100 % | 
| predict4All | Correction orthographique et prédiction du prochain mot | 100 % | 
| profil | Gestion du profil utilisateur | 50 % | 
| progress | Gestion de l'historique d'utilisation des outils | 57 % | 
| speech2Text | Générer une chaine de caractères à partir de l'audio | 100 % | 
| text2Speech | Générer l'audio à partir une chaine de caractères | 100 % | 
| windowManager | Liste fenêtres / applications ouvertes et focus | 57 % | 
| wordPrediction | Prédire la prochaine lettre | 100 % | 
