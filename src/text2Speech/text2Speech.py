#!/usr/bin/env python3
# coding: utf-8

'''
text to speech component

Licence    : MIT
Author     : Edouard Villain (evillain@lrtechnologies.fr) - LR Technologies
Created    : August 2023
Last update: Edouard Villain - January 2024
version    : v-1.0.0
'''

'''module text2Speech'''

import src.wrapper as wrapper 
from gtts import gTTS 
import playsound 
import os 
import pyttsx3 
import config  

# pyttsx3 is totally offline, but sound like hell 
# google tts is online but beautifull voice 
# both work but slow down application if we want 
# to generate sound after each key event 

class text2Speech():
    '''text2Speech : generate sound from text'''
    def __init__(self):
        '''init engine if we use pyttsx3'''

        self.file_path = config.base_path + "/src/text2Speech/tmp.mp3"
    
        #self.engine = pyttsx3.init()
        #self.engine.setProperty('voice', 'french')
    
    def tts(self, text, lang):
        '''tts : ask google to Text To Speech our text with specified langage'''
        try:                                                # pragma: no cover
            obj = gTTS(text=text, lang=lang, slow=False)    # pragma: no cover
            # save results and play it 
            obj.save(self.file_path)                        # pragma: no cover
            playsound.playsound(self.file_path)             # pragma: no cover
            os.remove(self.file_path)
        except:
            wrapper.main_app.logger.logger.debug(                # pragma: no cover
                    "[text2Speech] -  unable to generate {}".format(text))  # pragma: no cover
        
        # offline method, but sound like hell 
        #self.e.say(char)
        #self.e.runAndWait()
        
        # same as previous method, with default setting 
        # (no need to init engine, but slow)
        #pyttsx3.speak(char)