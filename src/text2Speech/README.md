# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : text2Speech   

#### Objectifs 

Utiliser la synthèse vocale pour générer le retour audio des caractères et chaines de caractères saises par l'utilisateur.  

#### Usages 

La fonction `tts()` accessible depuis le wrapper permet la synthèse vocale de bout en bout. 

* Utilisation du module `gtts` (Google Text To Speech)  
* Nécessité d'une connexion internet  
* `tts()` est exécutée uniquement si le retour audio est activé dans le profil de l'utilisateur  

```
if self.active_voice:
    wrapper.main_app.text2Speech.tts("Bonjour monde", 'fr')
```

#### Particularités 

* Une version locale de la synthèse vocale est disponible en commentaire (ne nécessitant pas internet)  

> La version utilisant internet génère un retour audio joli et proche d'un "humain" 
>> Retour audio utilisé dans google translate 
