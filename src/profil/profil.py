#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : June 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : profil -> load and save user and devices settings 
                @&&&&              #               
                   @&&             # 
'''

import src.wrapper as wrapper 
import json 
import config 


class profil:
    '''class profil'''
    def __init__(self):
        self.path_profil = config.base_path + '/src/profil/profil.json'
        self.user_name = 'Utilisateur'
    
    def load_profil(self):
        '''load profil'''
        with open(self.path_profil, 'r') as j:
            profil = j.read()
        self.profil = json.loads(profil)
    
    def save_profil(self, json_info):
        '''save profil'''
        with open(self.path_profil, 'w') as outfile:
            json.dump(json_info, outfile)

    def set_user(self, name):
        self.user_name = name 
        wrapper.main_app.logger.logger.debug("[profil] - user selected -> {}".format(self.user_name))

    def add_user(self, name):
        self.load_profil()
        json = self.profil
        json[self.user_name] = self.profil['Utilisateur']
        self.save_profil(json)
        wrapper.main_app.logger.logger.debug("[profil] - user added -> {}".format(self.user_name))

    def delete_user(self, name, lst_of_names):
        self.load_profil()
        json = {}
        for n in lst_of_names:
            json[n] = self.profil[n]
        self.save_profil(json)
        wrapper.main_app.logger.logger.debug("[profil] - user deleted -> {}".format(name))
        
    
    