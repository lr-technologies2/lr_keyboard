# LaLibelluleKeyBoard

Projet LR Technologies dont l'objectif est d'assister l'utilisation d'un ordinateur pour les personnes en situation de handicap. 

## Composant : profil 

#### Objectifs 

Gestion des paramètres utilisateurs de l'application.  

#### Usages 

Les paramètres utilisateurs sont stockés dans un fichier json.  

* Un paramètre utilisateurs du fichier json doit être modifiable dans l'IHM `userProfil.py` 
* Les paramètres sont lus et utilisés par l'application lors de l'initialisation de l'IHM, et à chaque appel de la fonction `resize()` 
* Les paramètres sont sauvegardés via le bouton `Sauvegarder` dans l'IHM `userProfil.py`  

```
wrapper.main_app.profil.load_profil() 
profil = wrapper.main_app.profil.profil

param_exemple = profil['exemple']
... 
```

#### Particularités 

Ajouter un paramètre utilisateur nécessite de :  

* Mettre à jour l'IHM `userProfil.py` pour que l'utilisateur puisse modifier le paramètre 
* Ajouter un champ dans le fichier json et une valeur par défaut 
* Ajouter une variable `ObjectProperty()` dans les IHM 
* Mettre à jour la lecture des paramètres dans les fonctions `__init__()` et `resize()` des IHM 
* Mettre à jour la fonction `save_profil()` du fichier `./src/IHM/userProfil.py`
