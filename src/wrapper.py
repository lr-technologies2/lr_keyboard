#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : April 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - November 2023
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : wrapper -> global access to all components 
                @&&&&              #               
                   @&&             # 
'''

import src.dummy.dummy as dummy 
import src.speechReco.speech2text as speech2text 
import src.logger.logger as logger 
import src.wordPrediction.wordPrediction as wordPrediction 
import src.nextWordPrediction.nextWordPrediction as nextWordPrediction 
import src.windowManager.windowManager as windowManager 
import src.keycodeEncoderDecoder.keycodeEncoderDecoder as keycodeEncoderDecoder 
import src.IHM_helper.IHM_helper as IHM_helper 
import src.predict4All.wordPredict4all as  wordPredict4all
import src.text2Speech.text2Speech as text2Speech 
import src.profil.profil as profil 
import src.progress.progress as progress
import src.deviceESP.deviceESP as deviceESP

import os.path
import subprocess
import config 


class wrapper:
    """wrapper class call from main in order to start and access component"""
    def __init__(self):
        """initialization of wrapper"""
        self.OS = config.OS
        """OS: user's plateform (Linux / Windows / MacOS)"""
        self.logger = logger.logger() 
        """logger: log for debugging application"""
        self.dummy = dummy.dummy(1)
        """dummy: dummy component"""
        self.speech2text = speech2text.speech2text()
        """speech2text: speech2text component"""
        self.wordPredict4all = wordPredict4all.wordPredict4all()
        """wordPredict4all: wordPredict4all component with 5 words"""
        self.nextWordPrediction = nextWordPrediction.nextWordPrediction(5)
        """nextWordPrediction: nextWordPrediction component with 5 words"""
        if self.OS == 'Windows':
            self.windowManager = windowManager.windowManager_Windows()
        else:
            self.windowManager = windowManager.windowManager_Linux()
        """windowManager: list all opened windows and set focus """
        self.IHM_helper = None
        """IHM_helper: kivy IHM"""
        self.keycodeEncoderDecoder = keycodeEncoderDecoder.keycodeEncoderDecoder()
        """keycodeEncoderDecoder : decode / encode keyboard event into char """
        self.text2Speech = text2Speech.text2Speech()
        """text2Speech : generate sound from text """
        self.profil = profil.profil()
        """profil : user profil options """
        self.progress = progress.progress()
        """progress : user progress """
        self.deviceESP = None
        """deviceESP : Device used by user """
        self.speech_recognized = ''


def compile_java():
    '''Compile a java file with subprocess'''
    if config.OS == 'Windows':
        subprocess.call(['javac', '-cp', './lib/*', '.\Predict4allConnector.java'], shell=False)
    elif config.OS == 'Linux':
        os.system('javac -cp \".:lib/*\" Predict4allConnector.java')

def execute_java ():
    '''Execute java with subprocess'''
    proc = None 
    if config.OS == 'Windows':
        proc = subprocess.Popen(['javaw', '-cp', '.;./lib/*', 'Predict4allConnector'], shell=False)
    elif config.OS == 'Linux':
        os.system('java -cp \".:lib/*\"  Predict4allConnector &')
    return proc 
        
    
def terminate_java_process(proc):
    '''kill process with pid'''
    if config.OS == 'Windows':
        os.system('taskkill /F /im javaw.exe')
    elif config.OS == 'Linux':
        os.system('pkill -f "java -cp .:lib/*"')

def launch_javaConnector():
    '''start java gateway'''
    # change dir inside predict4All component
    down = config.base_path + '/src/predict4All/'
    up = '../..'
    
    if config.deploy:
        if config.OS == 'Windows':
            up += '/..'
        elif config.OS == 'Linux':
            up += '/..'


    os.chdir(down)
    # compile and execute java gateway 
    compile_java()
    proc = execute_java()
    # get back to root 
    os.chdir(up)

    return proc 

def launch_deviceESP():
    """ Initialise and create ESP device"""
    main_app.deviceESP = deviceESP.deviceESP()

def main():
    global main_app 
    main_app = wrapper()
    
def launch_IHM():
    main_app.IHM_helper = IHM_helper.typeHelperApp() # pragma: no cover
    main_app.IHM_helper.run()
