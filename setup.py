#!/usr/bin/env python3
# coding: utf-8

'''
       /     &&                _     ____    _____         _                 _             _            
        *&&(&&#*              | |   |  _ \  |_   _|__  ___| |__  _ __   ___ | | ___   __ _(_) ___  ___  
          ,&&##%*             | |   | |_) |   | |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __| 
               #%%@.          | |___|  _ <    | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \ 
                 %%@(         |_____|_| \_\   |_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/ 
                 .#@@&                                                               |___/              
                   @@@/       
            .&.   (@@,//           ######################################################
           &&&&  &(@@@//           # Licence     : MIT
           && @/(/@@,///           # Author      : Edouard Villain (evillain@lrtechnologies.fr) 
           @  //(@@@@/*            # Created     : April 2023
       @@@@((/(@@@@@(#&%&&&        # Last update : Edouard Villain - January 2024
      @@@/(((// @@/*               # version     : v-1.0.0 
   %.@@@@@@/////&&                 #  
     &@&&  //  &,%&&               # module : setup script -> build executable for Linux and Windows
                @&&&&              #                            
                   @&&             # 
'''

from cx_Freeze import setup, Executable
import os, sys 

if __name__ == '__main__':
    base = None
    binpathincludes = []
    includefiles = []
    includes = []
    company_name = 'LR_Technologies'
    product_name = 'Koi rect me'
    if sys.platform == "linux":
        binpathincludes += ["/usr/lib"]
        includefiles += ['install.sh']
        packages = ['gi', 'gi._error', 'kivy._clock',  'kivy', 'py4j.java_collections']
        include_msvcr = False 
        add_to_path = False 
    if sys.platform == 'win32':
        base = 'Win32GUI'
        packages = ['kivy', 'py4j.java_collections', 'vosk.transcriber']
        #includefiles += ['post_install.py']
        includefiles += ['drivers.zip']
        includefiles += ['jdk-21_windows-x64_bin.exe']
        includefiles += [sys.prefix + '/share/glew/bin/glew32.dll']
        includefiles += [sys.prefix + '/share/sdl2/bin/SDL2.dll', \
                         sys.prefix + '/share/sdl2/bin/SDL2_mixer.dll', \
                         sys.prefix + '/share/sdl2/bin/SDL2_image.dll', \
                         sys.prefix + '/share/sdl2/bin/SDL2_ttf.dll']
        
        include_msvcr = True 
        add_to_path = True 
        shortcut_table = [
            ("DesktopShortcut",        # Shortcut
            "DesktopFolder",          # Directory_
            "Koï rect me",           # Name that will be show on the link
            "TARGETDIR",              # Component_
            "[TARGETDIR]main.exe",# Target exe to exexute
            None,                     # Arguments
            None,                     # Description
            None,                     # Hotkey
            None, #"logo_LR.ico",                     # Icon
            None,                     # IconIndex
            None,                     # ShowCmd
            'TARGETDIR'               # WkDir
            )
            ]

        # Now create the table dictionary
        msi_data = {"Shortcut": shortcut_table}
        
        bdist_msi_options = {
        'upgrade_code': '{66620F3A-DC3A-11E2-B341-002219E9B01E}',
        'add_to_path': False,
        'initial_target_dir': r'[ProgramFilesFolder]\%s\%s' % (company_name, product_name),
        'data':msi_data
        }
        
    
    excludes = []
    build_exe_options = {'includes':includes, 'excludes':excludes,'packages':packages,'include_files':includefiles, 'include_msvcr': include_msvcr, 'add_to_path': add_to_path}
    
    if sys.platform == 'win32':
        setup(name='Koi_rect_me',
            version     = '1.0.0',
            description = 'Smart assistant application',
            options     = {'build_exe': build_exe_options, 'bdist_msi':bdist_msi_options},
            executables = [Executable('main.py', copyright="Copyright (C) 2024 LR Technologies", base=base, icon="src/IHM_helper/img/logo_LR.ico")])
        
    if sys.platform == 'linux':
        setup(name='Koi_rect_me',
            version     = '1.0.0',
            description = 'Smart assistant application',
            options     = {'build_exe': build_exe_options},
            executables = [Executable('main.py', base=base, icon="src/IHM_helper/img/logo_LR.ico")])

# Command to run
# python setup.py build

# Output:
# A 'build' directory is created containing the standalone executable.